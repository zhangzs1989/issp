function Set_Path_Para()
try
    [tree, RootName , ~] = Read_xml('./config/config.xml');
    prompt0 = {                                                         % 对话框参数
        'Data folder located',tree.path.datapath
        'Phase file folder located',tree.path.phapath
        'Figure output path ',tree.path.fig_out
        'Result output path',tree.path.result_out
        };
    dlg0.width = 50;
    dlg0.title = 'Preprocessing';
    dlg0.auto = 0;
    para = Paradlg(prompt0,dlg0);
    try
        datapath = para{1};phapath = para{2};figpath=para{3};resultpath=para{4};
        if datapath(end)~='\'
            datapath=[datapath,'\'];
        end
        if phapath(end)~='\'
            phapath=[phapath,'\'];
        end
        if figpath(end)~='\'
            figpath=[figpath,'\'];
        end
        if resultpath(end)~='\'
            resultpath=[resultpath,'\'];
        end
    catch ErrorInfo
        msgbox(ErrorInfo.message)
    end
    try
        tree.path.datapath=datapath;
        tree.path.phapath=phapath;
        tree.path.fig_out=figpath;
        tree.path.result_out=resultpath;
        Write_xml('./config/config.xml',tree,RootName);
    catch ErrorInfo
        msgbox(ErrorInfo.message)
    end
end
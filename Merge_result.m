function Merge_result()
% [tree, ~ , ~] = Read_xml('./config/config.xml');
% resultpath = tree.path.result_out;
% disp(['result in ',resultpath,'\n']);
% files = dir([resultpath,'*.par']);
% currentpath = pwd;
% [filename,filepath] = uigetfile([datapath,'*','.par'],['Select an','*.par file']);
currentpath = pwd;
[filepath] = uigetdir(currentpath);
if ~isequal(filepath,0)
    files = dir([filepath,'\','*.par']);
    if ~isempty(files)
        outname = [datestr(now,'yyyymmddTHHMMSS'),'.out'];
        fresult = fopen([filepath,'\',outname],'a+');
        for i = 3:length(files)
            str = textread([files(i).folder,'\',files(i).name],'%s', 'delimiter', '\n', 'whitespace', '', ...
                'commentstyle', 'matlab');
            fprintf(fresult,'%s\n',str{1});
        end
        disp('Export Success!');
        disp(['Result is in ',filepath]);
        fclose(fresult);
    else
        disp(['There is no .par file in the ',filepath]);
    end
end
fclose('all');
end
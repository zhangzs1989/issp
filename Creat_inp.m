function Creat_inp()
% 匹配时间漫长。
% 使用波形内头信息的时间来创建输入文件,如果文件名是以yyyymmddHHMMSSxxxxx命名，可使用匹配文件名的形式来形成输入文件
% 由于目前波形文件名多以波形开始时间进行命名，震相文件以发震时刻进行命名，波形长短不一致，很难完全匹配两者
% 所以在FindSameFilename使用不同时间格式来进行匹配
% 得到的输出文件inp以当前计算机时间来命名，内容由于不是“完美匹配”，也许存在不合适的波形-震相对，请谨慎检查后使用。
% try
[tree, ~ , ~] = Read_xml('./config/config.xml');
wavetype  = tree.data.waveform;
inpfilename = ['.\pathinp\',datestr(now,'yyyymmddTHHMMSS'),'.inp'];
fid = fopen(inpfilename,'wt');
fprintf(fid,'%s\n%s\n%s\n%s\n','%%-- input filepath of seismic wave(.evt) and report(.pha)','%%-- 1st line is evtfile',...
    '%%-- 2nd line is reportfile','%%-----------------------------------------');
folder_name_evt = uigetdir('.\','输入波形文件所在文件夹');
index = findstr(folder_name_evt,'\');
folder_name_rpt = folder_name_evt(1:index(end));
folder_name_rpt = uigetdir(folder_name_rpt,'输入观测报告所在文件夹');
if folder_name_evt(end)~='\'
    folder_name_evt=[folder_name_evt,'\'];
end
if folder_name_rpt(end)~='\'
    folder_name_rpt=[folder_name_rpt,'\'];
end
switch wavetype
    case 1
        DIRS_evt=dir([folder_name_evt,'*.evt']);  %扩展名
    case 2
        DIRS_evt = dir(folder_name_evt);
end
h = waitbar(0,'正在生成，请稍后...','Positon',[1 1]);
DIRS_rpt=dir([folder_name_rpt,'*.pha']);  %扩展名
evtnum = length(DIRS_evt);rptnum=length(DIRS_rpt);
pp = 1;
for i = 3:evtnum
    waitbar(i / evtnum);
    switch wavetype
        case 1 % evt
            [~,head]=zjc_readevt([DIRS_evt(i).folder,'\',DIRS_evt(i).name]);
        case 2 % sac
            [~,head] = readsac_files([DIRS_evt(i).folder,'\',DIRS_evt(i).name]);
    end
    wavet = [head.begintime];
    for j = 1:rptnum
        T=pha2cell([DIRS_rpt(j).folder,'\',DIRS_rpt(j).name]);
        rptime = [T{1,1} T{1,2} T{1,3} T{1,4} T{1,5} T{1,6}];
        result = FindSameFilename(wavet,rptime,'yyyy-mm-dd');
        if result==1
            evtrpt{pp,1} = [folder_name_evt,DIRS_evt(i).name];
            evtrpt{pp,2} = [folder_name_rpt,DIRS_rpt(j).name];
            pp=pp+1;
        end
    end
end

fid = fopen(inpfilename,'a+');
for i=1:length(evtrpt)
    fprintf(fid,'%s\n%s\n',evtrpt{i,1},evtrpt{i,2});
end
close(h);
msgbox(['目录提取完成!','inp文件路径为:',inpfilename],'完成提示');
fclose('all');


% % %     n1=length(DIRS_evt);n2=length(DIRS_rpt);
% % %     pp = 1;
% % %     if n1 >= n2
% % %         for i = 1:n2
% % %             for j = 1:n1
% % %                 result = intersect(DIRS_evt(j).name,DIRS_rpt(i).name);
% % % %                 result=FindSameFilename(DIRS_evt(j).name,DIRS_rpt(i).name);
% % %                 if length(result)>=10
% % %                     evtrpt{pp,1} = [folder_name_evt,DIRS_evt(j).name];
% % %                     evtrpt{pp,2} = [folder_name_rpt,DIRS_rpt(i).name];
% % %                     pp=pp+1;
% % %                 end
% % %             end
% % %         end
% % %     else
% % %         for i = 1:n1
% % %             for j = 1:n2
% % % %                 result=FindSameFilename(DIRS_evt(i).name,DIRS_rpt(j).name);
% % %                 result = intersect(DIRS_evt(j).name,DIRS_rpt(i).name);
% % %                 if length(result)>=10
% % %                     evtrpt{pp,1} = [folder_name_evt,DIRS_evt(i).name];
% % %                     evtrpt{pp,2} = [folder_name_rpt,DIRS_rpt(j).name];
% % %                     pp=pp+1;
% % %                 end
% % %             end
% % %         end
% % %     end
%     h = waitbar(0,'正在生成，请稍后...','Positon',[1 1]);
%     fid = fopen(inpfilename,'a+');
%     for i=1:length(evtrpt)
%         fprintf(fid,'%s\n%s\n',strrep(evtrpt{i,1},'\','/'),strrep(evtrpt{i,2},'\','/'));
%         waitbar(i / length(evtrpt));
%     end
%         close(h);
%         msgbox(['目录提取完成!','inp文件路径为:',inpfilename],'完成提示');
%         fclose('all');
% catch
% end
end
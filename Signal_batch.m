function Signal_batch(hmain)
screen=get(0,'ScreenSize');
fig = hmain;
ax = axes(fig);
ax.Units = 'pixels';
ax.Position = [0.05*screen(3) 0.05*screen(4) 0.5*screen(3) 0.5*screen(4)];
ax.Box = 'on';
set(ax,'xtick',[]);
set(ax,'ytick',[]);
text(0.5,0.5,'results show in here.')
% try
[tree, ~ , ~] = Read_xml('./config/config.xml');
figpath = tree.path.fig_out;
resultpath = tree.path.result_out;
wavetype = tree.data.waveform;
[filename, pathname]  = uigetfile([cd,'\pathinp\','*.inp'],'选择数据输入文件.inp');
if isequal(filename,0)
    file=[];
    disp('输入inp文件不合适或者未选择！');
else
    file = textread([pathname,filename],'%s', 'delimiter', '\n', 'whitespace', '', ...
        'commentstyle', 'matlab');
end
if isempty(file)
    msgbox('check input file is or not empty');
else
    eqn  = numel(file)/2;eqn=floor(eqn);
    hw=waitbar(0,'Processing...');
    for eqi = 1:eqn
        try
        waitbar(eqi/eqn,hw,'Processing...');
        wavepath = char(file{(eqi-1)*2+1});
        rptpath = char(file{eqi*2});
        switch wavetype
            case 1
                [~,eq_label,~] = fileparts(wavepath);
                disp(['wave file name is ',wavepath]);
                [wave,head]=zjc_readevt(wavepath);
                stn = head.stn;t = table(stn,3);
            case 2
                [~,eq_label,~] = fileparts(wavepath);
                [wave,head]=readsac_files(wavepath);
                disp(['wave file name is ',eq_label]);
                %             eq_label = datestr(head.begintime,'yyyymmddHHMMSS');
                stn = head.stn;t = table(stn,3);
        end
        T=pha2cell(rptpath);
        value=T(1,:);pha=T(2:end,:);
        EVENT.origintime  = [value{1} value{2} value{3} value{4} value{5} value{6}];
        EVENT.epicenter   = [value{8} value{7} ];
        EVENT.depth       = [value{10}];
        EVENT.mag         = [value{9}];
        EVENT.loc_stn     = [];
        psnum = 1;
        [phanum,~]=size(pha);
        for i = 1:phanum
            if ~isempty(pha{i,2}) && ~isempty(pha{i,3})
                EVENT.STA(psnum).staname = (pha{i,1});
                EVENT.STA(psnum).Pg = datevec(pha{i,2});
                EVENT.STA(psnum).Sg = datevec(pha{i,3});
                psnum = psnum + 1;
            end
        end
        EVENT.loc_stn = length(EVENT.STA);
        
        preconditioning.gatten = tree.preprocessing.gatten;
        preconditioning.Qf = [tree.preprocessing.Qf_a,tree.preprocessing.Qf_b];
        preconditioning.instrumentcase = tree.preprocessing.resp;
        preconditioning.site = tree.preprocessing.site;
        preconditioning.model = tree.model;
        preconditioning.delay = tree.signal.delay ;
        preconditioning.noise = tree.signal.noise;
        preconditioning.swind = tree.signal.swind;
        preconditioning.fftnum = tree.signal.fftnum;
        preconditioning.waveform = tree.data.waveform;
        switch preconditioning.waveform
            case 1
                waveform = 'evt';
            case 2
                waveform = 'SAC';
            otherwise
                waveform = 'unknow';
        end
        preconditioning.wavetype = tree.signal.wavetype;
        switch preconditioning.wavetype
            case 1
                wave_type = 'P';
            case 2
                wave_type = 'SH';
            otherwise
                wave_type = 'unknow';
        end
        %             preconditioning.rptform = tree.data.reportform;
        preconditioning.invert = tree.invert;
        preconditioning.density = tree.ssp.density;
        preconditioning.velocity = tree.ssp.velocity;
        preconditioning.radiation = tree.ssp.radiation;
        SPECTRA = StationSpectra_z(wave,head,EVENT,preconditioning);
        SPECTRAnum = cellfun('isempty',SPECTRA);
        index = find(SPECTRAnum == 0);SPECTRA = SPECTRA(index(1:end-1));
        disp('station spectrum is ready...');
        switch preconditioning.model
            case 1 % Brune-2
                wavetitle= sprintf('eqlabel: %s; Seismic wave type: %s; Seismic wave format: %s; window = %ds',eq_label,wave_type,waveform,preconditioning.swind);
                filterrange = tree.signal.frerange;
                result = SpectraFit_Brune2(SPECTRA, preconditioning,eq_label,'v', filterrange,1);
                if ~isempty(result)
                    save_result(EVENT,result,[eq_label,'-B1'],resultpath)
                    resulttitle = sprintf('%s (@Frequency range. %s-%sHz; @eqinfo. %s)\n omg=%s;fc=%.2fHz;\n Mo=%sNm; Mw=%.2f; R=%.2fm;\n StressDrop=%sMPa; Apparent stress=%sMPa', ...
                        eq_label,num2str(filterrange(1)),num2str(filterrange(2)),...
                        [datestr(EVENT.origintime,31),',',num2str(EVENT.epicenter(1)),',',num2str(EVENT.epicenter(2)),',',num2str(EVENT.mag)],...
                        result.SPa(1,1),result.SPa(1,2),result.SSP(1),result.SSP(2),result.SSP(3),result.SSP(4),result.SSP(5));
                    h1001 = figure();screen = get(0,'ScreenSize');set(h1001,'Position',[screen(1) screen(2) screen(3)/1.2 screen(4)/1.2]);
                    ha = tight_subplot(2,2,[.1 .04],[.1 .05],[.05 .05]);
                    axes(ha(1)),wave_plot(SPECTRA,wavetitle);
                    axes(ha(2)),info_plot(SPECTRA,EVENT);axis equal;
                    axes(ha(3)),spec_plot(SPECTRA,'d',filterrange);%plotDiagnostics(result.mdl);
                    axes(ha(4)),fit_plot(result);px = xlim;py=ylim;
                    title(resulttitle);
                    set(gca,'LineWidth',1.5)
                    %                     title(resulttitle,'position',[(px(2)-px(1))/20 (py(2)-py(1))/6]);
                    %                     axes(ha(4)),axis off;text(0,0.5,resulttitle,'FontSize',12)
                    print(gcf,'-dpng','-r300',[figpath,eq_label,'.png']);
%                     saveas(gcf,[figpath,eq_label,'.fig']);
%                     close(h1001);
                    disp([eq_label,' work done!']);
                else
                    disp([eq_label,' is existing problem!']);
                end
            case 2 % Brune-gamma
                wavetitle= sprintf('eqlabel: %s; Seismic wave type: %s; Seismic wave format: %s; window = %ds',eq_label,wave_type,waveform,preconditioning.swind);
                filterrange = tree.signal.frerange;
                result = SpectraFit_Brune2(SPECTRA, preconditioning,eq_label,'v', filterrange,1);
                if ~isempty(result)
                    save_result(EVENT,result,[eq_label,'-B2'],resultpath)
                    resulttitle = sprintf('%s (@Frequency range. %s-%sHz; @eqinfo. %s)\n omg=%s;fc=%.2fHz;\n Mo=%sNm; Mw=%.2f; R=%.2fm;\n StressDrop=%sMPa; Apparent stress=%sMPa', ...
                        eq_label,num2str(filterrange(1)),num2str(filterrange(2)),...
                        [datestr(EVENT.origintime,31),',',num2str(EVENT.epicenter(1)),',',num2str(EVENT.epicenter(2)),',',num2str(EVENT.mag)],...
                        result.SPa(1,1),result.SPa(1,2),result.SSP(1),result.SSP(2),result.SSP(3),result.SSP(4),result.SSP(5));
                    h1001 = figure();screen = get(0,'ScreenSize');set(h1001,'Position',[screen(1) screen(2) screen(3)/1.2 screen(4)/1.2]);
                    ha = tight_subplot(2,2,[.1 .04],[.1 .05],[.05 .05]);
                    axes(ha(1)),wave_plot(SPECTRA,wavetitle);
                    axes(ha(2)),info_plot(SPECTRA,EVENT);axis equal;
                    axes(ha(3)),spec_plot(SPECTRA,'d',filterrange);%plotDiagnostics(result.mdl);
                    axes(ha(4)),fit_plot(result);px = xlim;py=ylim;
                    title(resulttitle);
                    set(gca,'LineWidth',1.5)
                    %                     title(resulttitle,'position',[(px(2)-px(1))/20 (py(2)-py(1))/6]);
                    %                     axes(ha(4)),axis off;text(0,0.5,resulttitle,'FontSize',12)
                    print(gcf,'-dpng','-r300',[figpath,eq_label,'.png']);
%                     saveas(gcf,[figpath,eq_label,'.fig']);
                    close(h1001);
                    disp([eq_label,' work done!']);
                else
                    disp([eq_label,' is existing problem!']);
                end
                
            case 3 % HC
                wavetitle= sprintf('eqlabel: %s; Seismic wave type: %s; Seismic wave format: %s; window = %ds',eq_label,wave_type,waveform,preconditioning.swind);
                filterrange = tree.signal.frerange;
                result = SpectraFit_HC2(SPECTRA, preconditioning,eq_label,'v', filterrange,1);
                if ~isempty(result)
                    save_result(EVENT,result,[eq_label,'-HC'],resultpath)
                    resulttitle = sprintf('%s (@Frequency range. %s-%sHz; @eqinfo. %s)\n omg=%s;fc=%.2fHz;\n Mo=%sNm; Mw=%.2f; R=%.2fm;\n StressDrop=%sMPa; Apparent stress=%sMPa', ...
                        eq_label,num2str(filterrange(1)),num2str(filterrange(2)),...
                        [datestr(EVENT.origintime,31),',',num2str(EVENT.epicenter(1)),',',num2str(EVENT.epicenter(2)),',',num2str(EVENT.mag)],...
                        result.SPa(1,1),result.SPa(1,2),result.SSP(1),result.SSP(2),result.SSP(3),result.SSP(4),result.SSP(5));
                    h1001 = figure();screen = get(0,'ScreenSize');set(h1001,'Position',[screen(1) screen(2) screen(3)/1.2 screen(4)/1.2]);
                    ha = tight_subplot(2,2,[.1 .04],[.1 .05],[.05 .05]);
                    axes(ha(1)),wave_plot(SPECTRA,wavetitle);
                    axes(ha(2)),info_plot(SPECTRA,EVENT);axis equal;
                    axes(ha(3)),spec_plot(SPECTRA,'d',filterrange);%plotDiagnostics(result.mdl);
                    axes(ha(4)),fit_plot(result);px = xlim;py=ylim;
                    title(resulttitle);
                    set(gca,'LineWidth',1.5)
                    %                     title(resulttitle,'position',[(px(2)-px(1))/20 (py(2)-py(1))/6]);
                    %                     axes(ha(4)),axis off;text(0,0.5,resulttitle,'FontSize',12)
                    print(gcf,'-dpng','-r300',[figpath,eq_label,'.png']);
%                     saveas(gcf,[figpath,eq_label,'.fig']);
                    close(h1001);
                    disp([eq_label,' work done!']);
                else
                    disp([eq_label,' is existing problem!']);
                end
        end
        catch
            
    end
    end
    
    
end
% catch exception

% end
end
function T = pha2cell(filename)
% T=cell{};
% [~,yyyy,mm,dd,HH,MM,SS,lon,lat,mag,depth] = textread( filename, '%s %d %d %d %d %d %.3f %.4f %.4f %.2f %.2f', 'delimiter', '\t' );
[~,yyyy,mm,dd,HH,MM,SS,lon,lat,mag,depth] = textread( filename, '%s %d %d %d %d %d %.2f %.4f %.4f %.2f %.1f', 1);
T(1,1)={yyyy};
T(1,2)={mm};
T(1,3)={dd};
T(1,4)={HH};
T(1,5)={MM};
T(1,6)={SS};
T(1,7)={lon};
T(1,8)={lat};
T(1,9)={mag};
T(1,10)={depth};
fid = fopen(filename,'r');
ii = 2;
while ~feof(fid)
    tline = fgetl(fid);
    if tline(1)~='#'
        str = regexp(tline,'\s+','split');
        if length(str)>=3
        T(ii,1)={str{1}};
        T(ii,2)={[str{2},' ',str{3}]};
        T(ii,3)={[str{4},' ',str{5}]};
        ii= ii+1;
        else
            T(ii,1)={str{1}};
            T(ii,2)={[]};
            T(ii,3)={[]};
            ii= ii+1;
        end
    end
end
fclose(fid);
% rawData = textread( filename, '%s', 'delimiter', '\t' );
% res = csv2cell( filename, delimiter )
end
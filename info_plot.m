function info_plot(SPECTRA,EVENT)
%   Detailed explanation goes here
try
SPECTRAnum = cellfun('isempty',SPECTRA);
index = find(SPECTRAnum == 0);SPECTRA = SPECTRA(index(1:end));
stn=numel(SPECTRA);
for i = 1:stn
    hsta = plot(SPECTRA{i}.staloc(2),SPECTRA{i}.staloc(1),'^','MarkerSize',6,'MarkerEdgeColor','k','MarkerFaceColor','k');hold on
    plot([EVENT.epicenter(2) SPECTRA{i}.staloc(2)],[EVENT.epicenter(1) SPECTRA{i}.staloc(1)],'-','color',[.7 .7 .7]);hold on
    text(SPECTRA{i}.staloc(2)-0.1,SPECTRA{i}.staloc(1)-0.1,SPECTRA{i}.staname);
end
    heq = plot(EVENT.epicenter(2),EVENT.epicenter(1),'p','MarkerSize',8,'MarkerEdgeColor','r','MarkerFaceColor','r');hold on
    lgd = legend([hsta,heq],'Station','Epicenter','Location','southeastoutside');
    title('Distribution of stations used')
    px=xlim;py=ylim;
    xlim([px(1)-0.2 px(2)+0.2]);ylim([py(1)-0.2 py(2)+0.2]);
    
catch
    msgbox('Spectra struct information may be empty.');
end
set(gca,'xaxislocation','top');  % 把x轴换到上方
% xlabel('Lontitude');ylabel('Latitude');
set(gca,'linewidth',1.2);
end


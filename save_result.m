function save_result(EVENT,result,eqlabl,path)
if ~isempty(result)
event = EVENT;
fc  = result.SPa(1,2);fc1=result.SPa(2,2);fc2=result.SPa(3,2);
omega  = result.SPa(1,1);omega1=result.SPa(2,1);omega2=result.SPa(3,1);
Mo=result.SSP(1);Mw=result.SSP(2);R=result.SSP(3);sd=result.SSP(4);app=result.SSP(5);rmes=result.rmes;
fp_par = fopen([path,eqlabl,'.par'],'w');
str_tmp = ['%%','Time, ','Lontitude, ','Latitude, ','Mag, ','Depth, ','omega',',','omega1',',','omega2',',','fc, ','fc1, ','fc2, ',...
    'Mo, ','Mw, ','Rupture(m), ','strssdrop(MPa)',',','apparent stress,','rmes'];
fprintf(fp_par,'%s\t\n',str_tmp);
fprintf(fp_par,'%s %.4f %.4f %.2f %.1f %.3e %.3e %.3e %.2f %.2f %.2f %.3e %.3f %.2f %.4f %.4f %.3f',...
    datestr(event.origintime,31),event.epicenter(2),...
    event.epicenter(1),event.mag,event.depth,omega,omega1,omega2,fc,fc1,fc2,Mo,Mw,R,sd,app,rmes);
fclose(fp_par);
else
   disp('result is null.') 
end
end
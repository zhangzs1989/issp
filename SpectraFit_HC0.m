function RESULT = SpectraFit_HC2(SPECTRA, preconditioning,eq_label,chrSpectype, fvrange,plot_or_not)
%               PROGRAM TO FIT SOURCE MODEL
% Brune model with high-cut fmax model fitted to source spectrum
% Input:
%	fv     - discrete frequencies.
%	spec   - observed spectra.
%	chrSpectype - 'v', 'd' or 'a'; default is 'v'. Correponding to spectra
%				   velocity, displacement or acceleration.
%	initValue   - initial values for theoretical model, a structure with 5
%				  members: .fc0, .fmax0, .index1, .index2, .slope;
%	plot_or_not - % =0, no plots; =1(default), plot figures.
% Output:
%	SPa    - spectra parameters, a structure.
%		.omega
%		.omega_intervals
%		.fc
%		.fc_intervals
%		.gamma
%		.gamma_intervals
%		.fmax
%		.fmax_intervals
%		.p
%		.p_intervals
%=========================================================================
% Jianchang ZHENG @ Jinan, Sept 15, 2015.
% Modified on Aug.14, 2015 @ Room 408# Plaza C, 2066 Ganxi Rd, Jinan.
% completed on May 26, 2016@ STEC, National Chung Cheng University.
% Modified by Zhengshuai Zhang,Nov 27,2021
RESULT=[];
SPECTRAnum = cellfun('isempty',SPECTRA);
index = find(SPECTRAnum == 0);SPECTRA = SPECTRA(index(1:end-1));
if sum(SPECTRAnum~=1)>= 4 %tree.data.stanum
    stn=numel(SPECTRA);
    staname=cell(stn,1);
    S_sta=[];
    DataV=0;
    NoiseV=0;
    for sti=1:stn
        NoiseV=NoiseV+SPECTRA{sti}.noiseV;
        DataV=DataV+SPECTRA{sti}.DataV;
        staname{sti}=SPECTRA{sti}.staname;
%         zr=zjc_Atkinson(SPECTRA{sti}.dist);
        S_sta=[S_sta SPECTRA{sti}.DataV(:,4)];
    end
    noisev = NoiseV./stn;
    datav=DataV./stn;
    datav(:,4)=median(S_sta,2);
    ind = 1:length(datav);
    S_sta= S_sta(ind,:);
    fv   = datav(ind,1);for ii = 1:stn fvv(:,ii) = fv;end
    spec  = datav(ind,4);noised = noisev(ind,4)./(2*pi*fv);
    
    Pi2=6.28318530718;
    SPa=[];
    nfv=numel(fv);
%     fun_HC = @(a,b,c,d,x) a./((1.+(x./b).^2).*(1+(x./c).^d)).^(-1);
    fun_HC = @(a,b,d,e,x) a./((1.+(x./b).^2).*(1+(x./d).^e));
    if ~exist('chrSpectype','var')
        chrSpectype='v'; %velocity
    end
    switch chrSpectype
        case 'a'
            acc  =spec;
            vel  =acc./(2*pi*fv).^1;
            displ=vel./(2*pi*fv).^1;
            tecc =vel.*(2*pi*fv).^2.5;
        case 'v'
            vel  =spec;
            displ=vel./(2*pi*fv).^1;
            acc  =vel.*(2*pi*fv).^1;
            tecc =vel.*(2*pi*fv).^2.5;
        case 'd'
            displ=spec;
            vel=displ.*(2*pi*fv).^1;
            acc  =vel.*(2*pi*fv).^1;
            tecc =vel.*(2*pi*fv).^2.5;
        otherwise
    end
    
    fp_xls=fopen(['./result/',eq_label,'.csv'],'w');
    str_tmp='Frequency,';
    for sti=1:stn
        str_tmp=[str_tmp sprintf('%s,',char(staname{sti}))];
    end
    str_tmp2 = [str_tmp,'Vel_AVG',',','Dis_AVG',',','Acc_AVG','Noise'];
    fprintf(fp_xls,'%s\n',str_tmp2);
    dlmwrite(['./result/',eq_label,'.csv'],[fv S_sta vel displ acc noised],'-append','precision', 6);
    fclose(fp_xls);
    
%     matfile=['.\init\' eq_label '.mat'];
%     if ~exist(matfile,'file')
%         [fc0,fmax0,index1,index2,slope]=zjc_initCornerPoints(fv,acc);
%         save(matfile,'fc0','fmax0','index1','index2','slope');
%     else
%         initValue = load(matfile);
%         fc0   =initValue.fc0;
%         fmax0 =initValue.fmax0;
%         index1=initValue.index1;
%         index2=initValue.index2;
%         slope =initValue.slope;
%     end
%     p0=abs(slope(3));
%     if ~exist('plot_or_not','var')
%         plot_or_not =0;  % =0, no plots;
%     end
    fc0=2;fmax0=12;p0=2;
    ind=fv<=fvrange(2) & fv>=fvrange(1);ind(1)=false;
    fv=fv(ind);vel=vel(ind);tecc=tecc(ind);acc = acc(ind);displ=displ(ind);noised=noised(ind);
    index1=find(abs(vel)==max(abs(vel)));
    fc=fv(index1,1);
    
    index2=find(abs(tecc)==max(abs(tecc)));
    fmax=fv(index2,1);
    
    avacc=acc(index1:index2);
    omega0=mean(avacc)/(2*pi*fc).^2;
    f1 = fv;d1=displ;
    g1 = @(a,b,d,e,x) log10(fun_HC(a,b,d,e,x));
%     modelfun =  @(coeffs,x) coeffs(1)./((1.+(x./coeffs(2)).^coeffs(3)).*(1.+(x./coeffs(4)).^coeffs(5)));
    beta0 = [omega0 fc0 fmax0 p0]; % An arbitrary guess
    [fitobj_1,gof,output] = fit(f1,log10(d1),g1,'StartPoint',beta0,...
        'Lower',[min(d1) min(f1)  10 2],...
        'Upper',[max(d1) max(f1)  max(f1) 5],...
        'Robust','LAR');
    CI = confint(fitobj_1,0.95);		 % 95% confidence intervals
    omega =fitobj_1.a;
    fc    =fitobj_1.b;
    gamma=2;
    fmax=fitobj_1.d;
    p=fitobj_1.e;
    omega_intervals =CI(:,1);
    fc_intervals    =CI(:,2);
%     gamma_intervals =CI(:,3);
    fmax_intervals =CI(:,3);
    p_intervals=CI(:,4);
    spektrd = fun_HC(omega,fc,fmax,p,fv);
    spektrd0 = fun_HC(omega,fc,fmax,p,fc);    
    spektrd1 = fun_HC(omega_intervals(1),fc_intervals(1),fmax_intervals(1),p_intervals(1),fv);
    spektrd2 = fun_HC(omega_intervals(2),fc_intervals(2),fmax_intervals(2),p_intervals(2),fv);
    
    SPa=[omega omega_intervals(1) omega_intervals(2); ...
        fc	   fc_intervals(1)    fc_intervals(2);    ...
        nan nan nan; ...
        fmax   fmax_intervals(1) fmax_intervals(2);  ...
        p    p_intervals(1)  p_intervals(2) ];
    SPa=SPa';
    [m0,mw,r,sd,app] = Sourceparameters_z(omega,fc,vel,preconditioning);
    SSP = [m0 mw r sd app];
    RESULT.SPa = SPa;
    RESULT.SSP = SSP;
    RESULT.obs = [fv displ];
    RESULT.noise = [fv noised];
    RESULT.the = [fv spektrd];
    RESULT.the1 = [fv spektrd1];
    RESULT.the2 = [fv spektrd2];
    RESULT.fc = [fc,spektrd0];
    RESULT.rmes = gof.rmse;
    RESULT.residuals = output.residuals;
else
    disp('station spectrum too litter.');
end
end
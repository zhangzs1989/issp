function [ result ] = FindSameFilename(wavet,rpt,format)
%UNTITLED Summary of this function goes here

switch format
    case 'yyyy-mm-dd'
        wtnum = datenum([wavet(1) wavet(2) wavet(3) 0 0 0]);
        rptnum = datenum([rpt(1) rpt(2) rpt(3) 0 0 0]);
        if wtnum==rptnum
            result = 1;
        else
            result = 0;
        end
        
    case 'yyyy-mm-dd HH:MM:SS'
        wtnum = datenum([wavet(1) wavet(2) wavet(3) wavet(4) wavet(5) wavet(6)]);
        rptnum = datenum([rpt(1) rpt(2) rpt(3) rpt(4) rpt(5) rpt(6)]);
        if wtnum==rptnum
            result = 1;
        else
            result = 0;
        end
        
    case 'yyyy-mm-dd HH'
        wtnum = datenum([wavet(1) wavet(2) wavet(3) wavet(4) 0 0]);
        rptnum = datenum([rpt(1) rpt(2) rpt(3) rpt(4) 0 0]);
        if wtnum==rptnum
            result = 1;
        else
            result = 0;
        end
        
    case 'yyyy-mm-dd HH:MM'
        wtnum = datenum([wavet(1) wavet(2) wavet(3) wavet(4) wavet(5) 0]);
        rptnum = datenum([rpt(1) rpt(2) rpt(3) rpt(4) rpt(5) 0]);
        if wtnum==rptnum
            result = 1;
        else
            result = 0;
        end
    otherwise
        disp('time format isnot standard.Please check it.');
end

end


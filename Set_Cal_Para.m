function Set_Cal_Para()
try
[tree, ~ , ~] = Read_xml('./config/config.xml');
prompt0 = {                                                         % 对话框参数
    'Model【1-\it{Brune(\omega^{-2})};2-\it{Brune2(w^{-\gamma}); 3-\it{High-Cut};}】',tree.model
    'Invert【1-\it{Least Square};】',tree.invert
    'Wave Format【1-\it{evt(only supported now)}; 2-\it{SAC}】',tree.data.waveform
    'Number of stations used【stationnum≥4】',tree.data.stanum
%     '观测报告格式【1-\it{全国编目系统产生的正式观测报告};2-\it{Jopens系统产生的观测报告}】',tree.data.reportform
    'Wave Type【1-\it{P}； 2-\it{S}】',tree.signal.wavetype
    'Noise length【\it{s}】',tree.signal.noise
    'P-wave offset【\it{s}】',tree.signal.delay
    'S-wave length【\it{s}，e.g. 5,10】',tree.signal.swind
    'FFT Points【e.g. 1024】',tree.signal.fftnum
    'Frequency Range【separated by spaces】',tree.signal.frerange
    'Density【\it{g/cm^3}】',tree.ssp.density
    'velocity【\it{km/s}】',tree.ssp.velocity
    'Radiation factor',tree.ssp.radiation
};
% defaultans = {'1','2','1','1','2','1024','2.67','3.2','0.41'};
dlg0.width = 80;
dlg0.title = 'Calculated parameters';
dlg0.auto = 0;
para = Paradlg(prompt0,dlg0);
try 
set_model = para{1};
if ismember(set_model,[1,2,3]) set_model = para{1};else msgbox('Model fail，default=1，check it！');set_model=1; end

set_fit = para{2};
if ismember(set_fit,[1,2,3]) set_fit = para{2};else msgbox('Invert mistake，default=1,check it！'); set_fit = 1;end

set_waveformat = para{3};
if ismember(set_waveformat,[1,2]) set_waveformat = para{3};else msgbox('Wave Format mistake，default=1,check it！'); set_waveformat = 1;end

set_stanum = para{4};   
if ismember(set_stanum,[1:1:999]) set_stanum = para{4}; else msgbox('number of station mistake，default=5,check it！');set_stanum=5; end

% set_rpttype = para{5}; 
% if ismember(set_rpttype,[1,2]) set_rpttype = para{5}; else msgbox('观测报告格式参数设置不正确，default=1,请检查！'); set_rpttype=1;end
set_wavetype = para{5};
if ismember(set_wavetype,[1,2]) set_wavetype = para{5}; else msgbox('wave type mistake，default=2,check it！');set_wavetype=2; end

set_noise = para{6};
if ismember(set_noise,[1:1:1000]) set_noise = para{6}; else msgbox('noise length mistake，default=2,check it！');set_noise=2; end

set_delay = para{7};
if ismember(set_delay,[0:0.1:5]) set_delay = para{7}; else msgbox('P wave offset mistake，default=1,check it！');set_delay=1; end

set_swind = para{8};
if ismember(set_swind,[1:1:1000]) set_swind = para{8}; else msgbox('S wave length mistake，default=10,check it！');set_swind=10; end

set_fftnum = para{9};
if ismember(set_fftnum,[128:2^20]) set_fftnum = para{9}; else msgbox('FFT points mistake，default=1,check it！');set_fftnum=1024; end
set_frerange = para{10};
if set_frerange(2)>set_frerange(1) set_frerange = para{10}; else msgbox('Frequency range mistake，default=[0 30],check it！');set_frerange=[0 30]; end

set_density = para{11};
set_velocity = para{12};
set_radiation = para{13};

catch ErrorInfo
    msgbox(ErrorInfo.message)
end
try
[tree, RootName , ~] = Read_xml('./config/config.xml');
tree.model = set_model;
tree.invert = set_fit;
tree.data.waveform = set_waveformat;
tree.data.stanum = set_stanum;
tree.signal.wavetype = set_wavetype;
tree.signal.noise = set_noise;
tree.signal.delay = set_delay;
tree.signal.swind = set_swind;
tree.signal.frerange=set_frerange;
tree.signal.fftnum = set_fftnum;
% tree.data.reportform = set_rpttype;
tree.ssp.density = set_density;tree.ssp.velocity = set_velocity;tree.ssp.radiation = set_radiation;
Write_xml('./config/config.xml',tree,RootName);
catch ErrorInfo
    msgbox(ErrorInfo.message)
end
catch  
end
end
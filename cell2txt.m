function cell2txt(fileName, datastruct,filePermissions)
%CELL2TXT Summary of this function goes here
%   Detailed explanation goes here
if ~exist('filePermissions', 'var')
    filePermissions = 'w';
end
datei = fopen(fileName, filePermissions);
eqinfo = datastruct.eqinfo;
pha = datastruct.pha;
[mm,nn]=size(eqinfo);if mm>nn ind=mm;else ind=nn;end
fprintf(datei,'%s\t', '#');
for i=1:ind
    fprintf(datei,'%s\t', num2str(eqinfo{i}));
end
fprintf(datei,'\n');
[mm,~]=size(pha);
for i = 1:mm
if ~isempty(pha{i,1})
% disp(i);
    fprintf(datei,'%s\t%s\t%s\n',pha{i,1},pha{i,2},pha{i,3});
end
end
fclose(datei);
end


function [x,y] = ginput_zoom()
%
% [x,y] = ginput_zoom()
%
%  You can zoom and pan before selecting each point,
%  press 'space' when you are done to start the selection
%
%  If you made a mistake, you can delete latest points pressing 'backspace'
% 
%  Once you are finished, press 'enter' to exit
%
%  Date: 27/01/2017
%  Authors: Martin Sanz Sabater (University of Valencia)
%           Javier Garcia Monreal (University of Valencia)
%
x=[];y=[];
h=[];
w=1;
while w~=0
  w = waitforbuttonpress;
  while w==0
    w = waitforbuttonpress;
  end
  cfg = gcf();
  ch = double(get(cfg, 'CurrentCharacter'));
  if ch == 13 % ENTER button
    break;
  end
  if ch == 8 % Backspace button
    if isempty(x) == 0
        x = x(1:end-1);
        y = y(1:end-1);
        delete(h(end));
        h = h(1:end-1);
        continue;
    end
  end
  
  [a,b]=ginput(2);
  x = [x;a];
  y = [y;b];
  hold on; h = [h;plot(x,y,'k+','MarkerSize',10)]; hold off;
end
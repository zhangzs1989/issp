function evt_path_name = Loadwave(wavetype)
% LOADWAVE Summary of this function goes here
% loadwave:导入波形数据路径和文件名
% 输入参数小于1个，默认波形格式.evt，目前仅支持evt格式
% 输入参数：1-evt、2-seed、3-SAC、4-ASCII文本文件（格式不统一，不建议使用）
wavetype=wavetype;
[tree, ~ , ~] = Read_xml('./config/config.xml');
datapath = tree.path.datapath;
evt_path_name=[];
try
    if nargin > 1
        msgbox('输入参数格式过多！', '提示');
    else
%         if nargin == 0
%             wavetype = 1;
%             [filename,filepath] = uigetfile('.evt','Select an evt file');
%         end
        if nargin == 1
%             wavetype = varargin{1};
            switch wavetype
                case 1
                    type =  '.evt';
                    [filename,filepath] = uigetfile([datapath,'*',type],['Select an',type,'file']);
                    if ~isequal(filename,0) && ~isequal(filepath,0)
                        %                 msgbox('获取波形文件路径成功！','操作提示')
                        file_path_name = [filepath,filename];
                        disp(filename);
                    else
                        %                 warndlg('获取波形文件失败','操作提示')
                    end
                case 2
                    type =  '.sac';
%                     [filename,filepath] = uigetfile(type,['Select an folder include ',type,'files']);
                    file_path_name = uigetdir(datapath);
                    disp(file_path_name);
                otherwise
                    type = 'unknow wave type.';
            end
            
        end
        
    end
    evt_path_name = file_path_name;
    save('./temp/evt_path_name.mat','evt_path_name');
catch
end
end



function wave_plot(SPECTRA,title_str)
% cmenu = uicontextmenu;
% for i = 1:length(SPECTRA)
%    staname{i} = SPECTRA{i}.staname; 
% end
% screen=get(0,'ScreenSize');
% W=screen(3);H=screen(4);
% main_p = [0.1*W,0.1*H,0.8*W,0.8*H];
% hw  = figure('Color',[255/255 243/255 219/255],'Position',main_p);
loc = 1;
for i = 1:length(SPECTRA) 
%     disp(i)
    dt = (datenum(SPECTRA{i}.Pgtime)-datenum(SPECTRA{i}.wavebegintime))*24*3600;
    if dt>=10 && (SPECTRA{i}.t(end)+10<SPECTRA{i}.to(end))
        t1 = round(dt-10);t2=round(SPECTRA{i}.t(end)+10);
        ind1 = find(SPECTRA{i}.to>=t1,1,'first');if isempty(ind1) ind1 = ftind(SPECTRA{i}.to>=SPECTRA{i}.to(1),1,'first'); end
        ind2 = find(SPECTRA{i}.to>=t2,1,'first');if isempty(ind2) ind2 = find(SPECTRA{i}.to>= SPECTRA{i}.to(end),1,'first'); end
        plot(SPECTRA{i}.to(ind1:ind2),2.5*loc+SPECTRA{i}.do(ind1:ind2,1)./max(abs(SPECTRA{i}.do(:,1))),'color',[0.7 0.7 0.7],'LineWidth',1.2);
        hold on
        plot(SPECTRA{i}.t,2.5*loc+SPECTRA{i}.d(:,1)./max(abs(SPECTRA{i}.d(:,1))),'color','r','LineWidth',1.2);
%         xlim([dt-10  SPECTRA{i}.t(end)+10])
    else
        t1 = round(SPECTRA{i}.to(1));t2=round(SPECTRA{i}.t(end)+10);
        ind1 = find(SPECTRA{i}.to>=t1,1,'first');if isempty(ind1) ind1 = ftind(SPECTRA{i}.to>=SPECTRA{i}.to(1),1,'first'); end
        ind2 = find(SPECTRA{i}.to>=t2,1,'first');if isempty(ind2) ind2 = find(SPECTRA{i}.to>= SPECTRA{i}.to(end),1,'first'); end
        plot(SPECTRA{i}.to(ind1:ind2),2.5*loc+SPECTRA{i}.do(ind1:ind2,1)./max(abs(SPECTRA{i}.do(:,1))),'color',[0.7 0.7 0.7],'LineWidth',1.2);
        hold on
        plot(SPECTRA{i}.t,2.5*loc+SPECTRA{i}.d(:,1)./max(abs(SPECTRA{i}.d(:,1))),'color','r','LineWidth',1.2);
%         xlim([dt SPECTRA{i}.t(end)+10])
    end
    
%     xlim([fix((SPECTRA{i}.t(1)-SPECTRA{i}.to(1))/5) SPECTRA{i}.t(end)+fix((SPECTRA{i}.to(end)-SPECTRA{i}.t(end))/5)])
    text(SPECTRA{i}.to(ind2)+1,loc*2.5,SPECTRA{i}.staname,'color','k');% Net.Station
    
    loc = loc + 1;
end
set(gca,'yticklabel',[]);
set(gca,'linewidth',1.2);
title(title_str);
xlabel('Time[s]'); ylabel('Velocity');box on;
% % % plot(SPECTRA{1}.to,SPECTRA{1}.do(:,1),'color',[0.7 0.7 0.7]);
% % % hold on
% % % plot(SPECTRA{1}.t,SPECTRA{1}.d(:,1),'color','r');
% % % rectangle('Position', [SPECTRA{1}.t(1) -max(max(abs(SPECTRA{1}.do(:,1)))) ...
% % %     SPECTRA{1}.t(end)-SPECTRA{1}.t(1) 2*max(max(abs(SPECTRA{1}.do(:,1))))],'FaceColor','g'...
% % %                ,'EdgeColor','red','LineWidth',1);
% % % plot(SPECTRA{1}.t,SPECTRA{1}.d(:,1),'color','r');
% % % xlim([0 ])

% % % pp=gca;
% % % c = uicontextmenu;
% % % pp.UIContextMenu = c;
% % % for j = 1:length(staname)
% % %     name = ['m',num2str(j)];
% % %     name = uimenu(c,'Label',staname{j},'Callback',@setlinestyle);
% % % end
% m1 = uimenu(c,'Label','dashed','Callback',@setlinestyle);

% % % function setlinestyle(source,callbackdata)
% % %     cla reset
% % %     pp = gca;
% % %     for k = 1:length(SPECTRA)
% % %        if strcmp(SPECTRA{k}.staname,callbackdata.Source.Label)
% % %            plot(SPECTRA{k}.to,SPECTRA{k}.do(:,1),'color',[0.8 0.8 0.8]);
% % %            hold on
% % %            rectangle('Position', [SPECTRA{k}.t(1) -max(max(abs(SPECTRA{k}.do(:,1)))) ...
% % %     SPECTRA{k}.t(end)-SPECTRA{k}.t(1) 2*max(max(abs(SPECTRA{k}.do(:,1))))],'FaceColor','g'...
% % %                ,'EdgeColor','red','LineWidth',1);
% % %            plot(SPECTRA{k}.t,SPECTRA{k}.d(:,1),'color','r');
% % %            xlabel('Time/s'); ylabel('Amplitude');box on;
% % %            title ([char(SPECTRA{k}.staname),',','BeginTime:',datestr(SPECTRA{k}.wavebegintime,31)]);
% % %        end
% % %     end
% % % end
end
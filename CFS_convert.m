function CFS_convert()
% 将国家台网中心编目系统的CFS进行信息抽取，得到适用于本程序的震相报告.csv
% 适用于单个地震观测报告和多个地震的综合报告转换
% 导出的格式存储在/time/文件夹下，以时间命名
% try
[tree, ~ , ~] = Read_xml('./config/config.xml');
phapath = tree.path.phapath;
selpath = uigetdir('./','Select an phase report file');
% [filename,filepath] = uigetfile('.txt','Select an phase report file');
if isequal(selpath,0)
    disp('filder selected is fail.');
else
    files = scanDir(selpath,'.txt');
    for i = 1:length(files)
        Info = ReadReport_for_PgSg(files{i});
        for j = 1:length(Info.event)
            eq_str = datestr(Info.event(j).origintime,30);
            ind = (eq_str=='T');ind=~ind;
            eq_label = eq_str(ind);
            eqinfo(1) ={Info.event(j).origintime(1)};
            eqinfo(2) ={Info.event(j).origintime(2)};
            eqinfo(3) ={Info.event(j).origintime(3)};
            eqinfo(4) ={Info.event(j).origintime(4)};
            eqinfo(5) ={Info.event(j).origintime(5)};
            eqinfo(6) ={Info.event(j).origintime(6)};
            eqinfo(7) ={Info.event(j).epicenter(2)};
            eqinfo(8) ={Info.event(j).epicenter(1)};
            eqinfo(9) ={Info.event(j).mag};
            eqinfo(10)={Info.event(j).depth};
            pha=cell(Info.event(j).loc_stn,10);
            for k = 1:length(Info.event((j)).STA)
                if ~isempty(Info.event(j).STA(k).Pg) && ~isempty(Info.event(j).STA(k).Sg)
                    pha{k,1}=Info.event(j).STA(k).staname;
                    pha{k,2}=datestr(Info.event(j).STA(k).Pg,31);
                    pha{k,3}=datestr(Info.event(j).STA(k).Sg,31);
                else
                    pha{k,1}=Info.event(j).STA(k).staname;
                    pha{k,2}={[]};
                    pha{k,3}={[]};
                end
            end
            value_struct.eqinfo=eqinfo;
            value_struct.pha=pha;
            cell2txt([phapath,eq_label,'.pha'],value_struct,'w');           
        end
        
    end
    
end

% catch

% end

end
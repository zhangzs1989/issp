function [pgt,sgt] = markps()
global pgt sgt ind
[PM,~ ]= ginput(2);
Mp=round(PM(1));
Ms=round(PM(2));
pgt=num2str(Mp);
sgt=num2str(Ms);
% save('temp_ps.nat','pgt','sgt');
end
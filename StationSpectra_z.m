function SPECTRA = StationSpectra_z(WAVE,WAVEHEAD,EVENT,Precondition)
WAVE = WAVE;WAVEHEAD = WAVEHEAD;EVENT = EVENT;preconditioning = Precondition;
%% 预处理参数
Qf = preconditioning.Qf;
gatten = preconditioning.gatten;
resp = preconditioning.instrumentcase;
site = preconditioning.site;
% filterpath = preconditioning.filter;
%% 计算参数
model = preconditioning.model;
delay = preconditioning.delay;
noise = preconditioning.noise; 
stwind = preconditioning.swind; %截取5s的s波长度
fftnum = preconditioning.fftnum;
waveform = preconditioning.waveform;
wavetype = preconditioning.wavetype;
% rptform = preconditioning.rptform;
invert = preconditioning.invert;
density = preconditioning.density;
velocity = preconditioning.velocity;
radiation = preconditioning.radiation;
delay = preconditioning.delay;
%%
N = fftnum;
SPECTRA{WAVEHEAD.stn}.staname = '';
SPECTRA{WAVEHEAD.stn}.dist    = nan;
SPECTRA{WAVEHEAD.stn}.az      = nan;
SPECTRA{WAVEHEAD.stn}.DataV   = NaN(N,4);
SPECTRA{WAVEHEAD.stn}.DataD   = NaN(N,4);
SPECTRA{WAVEHEAD.stn}.DataA   = NaN(N,4);
SPECTRA{WAVEHEAD.stn}.noiseV  = NaN(N,4);
SPECTRA{WAVEHEAD.stn}.noiseD  = NaN(N,4);
SPECTRA{WAVEHEAD.stn}.noiseA  = NaN(N,4);
%-NxM逻辑零的矩阵-%
sti_bad = false(WAVEHEAD.stn,1);
sti_bad = false(WAVEHEAD.stn,1);
for sti = 1:WAVEHEAD.stn
    ifound      = false;
    station     = [WAVEHEAD.spara(sti).sta_lat,WAVEHEAD.spara(sti).sta_lon];
    staname     = char(WAVEHEAD.spara(sti).staname);
%     temp = strsplit(staname,'/');staname = temp{2}
    %-方位角 & 震中距 -%
    [dist,az]   = distance(EVENT.epicenter,station);
    dist        = distdim(dist,'deg','km');Rij = sqrt(dist*dist+EVENT.depth*EVENT.depth);
    samplerate  = fix(WAVEHEAD.spara(sti).sample);
    [~,wavenn]=size(WAVE{sti});
    if wavenn == 3
    y=WAVE{sti}(:,1); y=y-mean(y);  % N-S
    x=WAVE{sti}(:,2); x=x-mean(x);  % E-W
    z=WAVE{sti}(:,3); z=z-mean(z);  % U-D
    x1 = x;y1=y;z1=z;
    else
       continue; 
    end
x = detrend(x);y = detrend(y);z = detrend(z);
%             x = bandpass(x,[0.01 30],samplerate);
%             y = bandpass(y,[0.01 30],samplerate);
%             z = bandpass(z,[0.01,30],samplerate);
            
    %---------------------------------------------------------------
    %   STEP-1      ROTATION ABOUT Z-AXIS CLOCKWISE AXIS
    %---------------------------------------------------------------
    theta1=deg2rad(az);
    x1=(x.*cos(theta1))-(y.*sin(theta1));
    y1=(x.*sin(theta1))+(y.*cos(theta1));
    z1=z;
    rdata=[x1,y1,z1];
    % -------------------------------------------------------------------------
    %       STEP-2 INSTRUMENT RESPONSE CORRECTION
    %__________________________________________________________________________
    %INSTRUMENT CONSTANT= [{SENSITIVITY (V/m/s)}/{GEN CONSTT (V/Counts)}]*100
    %         instrumentcase = 2;
    switch resp
        case 2
            for i=1:3
                A=3.9844e-07; %A=1.6e-07; % Converts counts to cm /sec
                CX=rdata(:,i)*A;
                p=[-.707+.707j ; -.707-.707j ; -62.3816+135.392j ; -62.3816-135.392j ; -350 ; -75 ];
                z=[0;0];
                [b,a]=zp2tf(p,z,1.7071e-009);
                sys=tf(b,a);
                %                 bode(sys);
                VDATA(:,i)=filter(b,a,CX);
            end
        case 1
            unit = 1;
            switch unit
                case 1  % raw data unit is um/s
                    VDATA=rdata./10000;  % convert to cm/s
            end
    end
    VX=VDATA(:,1);
%     switch wavetype
%         case 2
%             if tp*samplerate>length(VX)
%                 
%             else
%                 
%             end
%         case 1
%     end

    for stj=1:EVENT.loc_stn  % stj 观测报告中的台站编号
        %-波形中的台站名是否存在在观测报告中，存在ifound=true，初始ifound=flase;
        if strfind(staname,EVENT.STA(stj).staname)
            ifound=true;
            STANAME = EVENT.STA(stj).staname;
            break;
        end
    end
    clear ii;
    switch wavetype
        case 2  % 仅对SH分量进行震源谱计算
            if ifound
%                 N = fftnum*WAVEHEAD.spara(stj).sample;
                info = sprintf('The station is %s',STANAME);
                disp(info);
                Pg=EVENT.STA(stj).Pg;
                Sg=EVENT.STA(stj).Sg;
                tp=(datenum(Pg)-datenum(WAVEHEAD.begintime))*24*3600;
                ts=(datenum(Sg)-datenum(WAVEHEAD.begintime))*24*3600;
                tp=roundn(tp,1);ts=roundn(ts,1);
                if ts>0 && (ts*samplerate+stwind*samplerate-1)<=WAVEHEAD.recordlength*samplerate
                    M=round(ts*samplerate);
                    t=(0:1/samplerate:(length(VX)-1)*(1/samplerate))';
                    tsp=(ts-tp);
                    tsr=tsp+1.38*tsp;%
%                     gr=tsp*8*100000;%
                    gr=Rij;
%                     zr=zjc_Atkinson(SPECTRA{sti}.dist);
                    %--------------------------------------------------------------------------
                    %       STEP3           WINDOW FUNCTION
                    %--------------------------------------------------------------------------
                    w=kaiser(stwind*samplerate);%     COSINE TAPER  (20%)tukeywin(N,0.2);
                    if M+stwind*samplerate-1>size(VDATA,1) % 起始点+fftN>数据总长度？补零
                        VDATA=[VDATA;zeros(M+stwind*samplerate-1-size(VDATA,1),3)];
                    end
                    for i=1:3
                        wdata(:,i)=VDATA(M:M+stwind*samplerate-1,i).*w;
                    end
                    n=N;
                    f=(1:n)*(samplerate/N);
                    datav(:,1)=f;
                    datad(:,1)=f;
                    dataa(:,1)=f;
                    if tp-noise>0 %noise level
                        noiseV(:,1)=f;noiseD(:,1)=f;noiseA(:,1)=f;
                        if tp*samplerate<length(VX) && (tp-5)*samplerate>0
                            for i = 1:3
                        noisewave = VX((tp-5)*samplerate:tp*samplerate);
                        bnoise = abs(fft(noisewave.*kaiser(length(noisewave)),(fftnum)))*2/(fftnum); 
                        bnoise=bnoise*gr;
                        noiseV(:,i+1) = bnoise;
                        noiseD(:,i+1) = bnoise./(2*pi*f');
                        noiseA(:,i+1) = bnoise.*(2*pi*f');
                            end 
                        else 
                        break;
                        end
                    else
                        noisewave = VX(1:tp*samplerate);
                        for i = 1:3
                        bnoise = abs(fft(noisewave.*kaiser(length(noisewave)),(fftnum)))*2/(fftnum); 
                        bnoise=bnoise*gr;
                        noiseV(:,i+1)= bnoise;
                        noiseD(:,i+1) = bnoise./(2*pi*f');
                        noiseA(:,i+1) = bnoise.*(2*pi*f');
                        end
                    end
                    %--------------------------------------------------------------------------
                    %       STEP4          Anelastic attenuation adjesting弹性衰减
                    %--------------------------------------------------------------------------
                    for i=1:3
                        df=wdata(:,i);
                        b=fft(df,N);
                        b=abs(b(1:n))./n;
                        switch site
                            case 1
                                b=b./2;%场地响应
                            case 2
                                msgbox('暂不支持','操作提示')
                            otherwise
                        end
                        %-Anelastic attenuation adjesting弹性衰减-%
                        q = Qf(1).*f.^Qf(2);
                        atnn=exp(-(pi*f*tsr)./q);%a=exp(nu./de)
                        bb1=b'./atnn;
                        switch gatten
                            case 1
                                bb=bb1.*gr*100000; % 几何衰减
                            case 2
                                zr=zjc_Atkinson(gr,Precondition);
                                bb=bb1.*zr*100000;
                            otherwise
                        end
                        datav(:,i+1)=bb;
                        datad(:,i+1)=bb./(2*pi*f);
                        dataa(:,i+1)=bb.*(2*pi*f);
                    end
                    SPECTRA{sti}.DataV   = datav;
                    SPECTRA{sti}.DataD   = datad;
                    SPECTRA{sti}.DataA   = dataa;
                    SPECTRA{sti}.noiseV   = noiseV;
                    SPECTRA{sti}.noiseD   = noiseD;
                    SPECTRA{sti}.noiseA   = noiseA;
                    
                    SPECTRA{sti}.staname = STANAME;
                    SPECTRA{sti}.dist    = dist;
                    SPECTRA{sti}.az      = az;
                    SPECTRA{sti}.staloc = [WAVEHEAD.spara(sti).sta_lat,WAVEHEAD.spara(sti).sta_lon];
                    SPECTRA{sti}.Pgtime        = Pg;
                    SPECTRA{sti}.Sgtime        = Sg;
                    SPECTRA{sti}.starttime     = EVENT.origintime;
                    SPECTRA{sti}.wavebegintime = WAVEHEAD.begintime;
                    SPECTRA{sti}.wavelength    = WAVEHEAD.recordlength;
                    SPECTRA{sti}.precondition  = preconditioning;
                    SPECTRA{sti}.to  = t;
                    SPECTRA{sti}.do  = VDATA;
                    SPECTRA{sti}.t  = t(M:M+stwind*samplerate-1);
                    SPECTRA{sti}.d  = VDATA(M:M+stwind*samplerate-1,:);
                    
                    clear datav datad dataa f
                else
                    continue;
                end     
            else
                sti_bad(sti)=true;
            end%ifound
        case 1 %P：Pg至Sg时间段；SH：
            if ifound
                Pg=EVENT.STA(stj).Pg;
                Sg=EVENT.STA(stj).Sg;
                tp=(datenum(Pg)-datenum(WAVEHEAD.begintime))*24*3600;
                ts=(datenum(Sg)-datenum(WAVEHEAD.begintime))*24*3600;
                if tp>0
                    tp=tp-delay;
                    M=round(ts*samplerate);
                    Mp = round(tp*samplerate);
                    t=(0:1/samplerate:(length(VX)-1)*(1/samplerate))';
                    tsp=(ts-tp);
                    tsr=tsp+1.38*tsp;%
                    gr=Rij;%几何扩散估计，8km/s
                    %--------------------------------------------------------------------------
                    %       STEP3           WINDOW FUNCTION
                    %--------------------------------------------------------------------------
                    %     COSINE TAPER  (20%)tukeywin(N,0.2);
                    w=kaiser(length(VDATA(Mp+1:M-1,1)));
                    for i=1:3
                        wdata(:,i)=VDATA(Mp+1:M-1,i).*w;
                    end
                    n=N/2;
                    f = (1:n)*(samplerate/N);
                    datav(:,1) = f;
                    datad(:,1) = f;
                    dataa(:,1) = f;
                    if tp-noise>0 %noise level
                        noiseV(:,1)=f;noiseD(:,1)=f;noiseA(:,1)=f;
                        for i = 1:3
                        noisewave = VX((tp-5)*samplerate:tp*samplerate);
                        bnoise = abs(fft(noisewave.*kaiser(length(noisewave)),(fftnum)))*2/(fftnum); 
%                         bnoise=bnoise*gr;
                        noiseV(:,i+1) = bnoise;
                        noiseD(:,i+1) = bnoise./(2*pi*f');
                        noiseA(:,i+1) = bnoise.*(2*pi*f');
                        end
                    else
                        noisewave = VX(1:tp*samplerate);
                        for i = 1:3
                        bnoise = abs(fft(noisewave.*kaiser(length(noisewave)),(fftnum)))*2/(fftnum); 
%                         bnoise=bnoise*gr;
                        noiseV(:,i+1)= bnoise;
                        noiseD(:,i+1) = bnoise./(2*pi*f');
                        noiseA(:,i+1) = bnoise.*(2*pi*f');
                        end
                    end
                    %--------------------------------------------------------------------------
                    %       STEP4          Anelastic attenuation adjesting 弹性衰减
                    %--------------------------------------------------------------------------
                    for i=1:3
                        df=wdata(:,i);
                        b=fft(df,N);
                        b=2*abs(b(1:n))./n;
                        switch site
                            case 1
                                b=b/1.33;%场地响应
                            case 2
                                msgbox('暂不支持','操作提示')
                            otherwise
                        end
                        %-Anelastic attenuation adjesting弹性衰减-%
                        %             q = 552.7.*f.^0.2801;
                        q = Qf(1).*f.^Qf(2);
                        atnn=exp(-pi*f*tsr./q);%a=exp(nu./de)
                        bb1=b'./atnn;
                        switch gatten
                            case 1
                                bb=bb1*gr*100000; % 几何衰减
                            case 2
                                zr=zjc_Atkinson(gr,Precondition);
                                bb=bb1.*zr*100000;
                            otherwise
                        end
                        datav(:,i+1)=bb;
                        datad(:,i+1)=bb./(2*pi*f);
                        dataa(:,i+1)=bb.*(2*pi*f);
                    end
                    SPECTRA{sti}.DataV   = datav;
                    SPECTRA{sti}.DataD   = datad;
                    SPECTRA{sti}.DataA   = dataa;
                    SPECTRA{sti}.noiseV   = noiseV;
                    SPECTRA{sti}.noiseD   = noiseD;
                    SPECTRA{sti}.noiseA   = noiseA;
                    
                    SPECTRA{sti}.staname = STANAME;
                    SPECTRA{sti}.dist    = dist;
                    SPECTRA{sti}.az      = az;
                    SPECTRA{sti}.staloc = [WAVEHEAD.spara(sti).sta_lat,WAVEHEAD.spara(sti).sta_lon];
                    SPECTRA{sti}.Pgtime        = Pg;
                    SPECTRA{sti}.Sgtime        = Sg;
                    SPECTRA{sti}.starttime     = EVENT.origintime;
                    SPECTRA{sti}.wavebegintime = WAVEHEAD.begintime;
                    SPECTRA{sti}.wavelength    = WAVEHEAD.recordlength;
                    SPECTRA{sti}.precondition  = preconditioning;
                    SPECTRA{sti}.to  = t;
                    SPECTRA{sti}.do  = VDATA;
                    SPECTRA{sti}.t  = t(Mp+1:M);
                    SPECTRA{sti}.d  = VDATA(Mp+1:M,:);
                    
                    clear datav datad dataa f
                else
                    sti_bad(sti)=true;
                end %ifound
            else
                continue;
            end
    end
    clear wdata VDATA;
end %sti
SPECTRA(sti_bad)=[];
end
function [wave,head] = readsac_files(files_path)
[files] = scanDir(files_path,'sac');
for i = 1:length(files)
    newpath = replace(files{i},'/','\');
    str1 = strsplit(newpath,'\');
    str2 = strsplit(str1{end},'.');
    [sachd,~ ] = irdsac(newpath);
    list(i)={[deblank(sachd.knetwk'),'_',deblank(sachd.kstnm')]};
end
[C,~,~] = unique(list);
stn  = length(C);
% str1 = strsplit(files{1},'\');
% str2 = strsplit(str1{end},'.');
% [month_of_doy, day_of_doy] = Doy2Date(str2num(str2{1}),str2num(str2{2}));
% hour = str2num(str2{3})+8;minuter = str2num(str2{4});sec = str2num(str2{5});
% HEAD.begintime = [str2num(str2{1}),month_of_doy,day_of_doy,hour,minuter,sec];
% HEAD.stn = length(files);
i = 1;
for stni = 1:stn
    index = find(strcmp(list,C{stni}));
    if length(index)==3
        newpath = replace(files{index(1)},'/','\');
        str1 = strsplit(newpath,'\');
        str2 = strsplit(str1{end},'.');
        [sachd,~ ] = irdsac(newpath);
        sacdata1 = rsac(newpath);
        newpath = replace(files{index(2)},'/','\');
        sacdata2 = rsac(newpath);
        newpath = replace(files{index(3)},'/','\');
        sacdata3 = rsac(newpath);
        WAVE{i,1} = [sacdata1(:,2),sacdata2(:,2),sacdata3(:,2)]./sachd.scale*1000;
        [month_of_doy, day_of_doy] = Doy2Date(sachd.nzyear,sachd.nzjday);
        hour = (sachd.nzhour)+8;minuter = (sachd.nzmin);sec = (sachd.nzsec)+(sachd.nzmsec)/1000;%um/s
        HEAD.begintime = [sachd.nzyear,month_of_doy,day_of_doy,hour,minuter,sec];
        recordlength = length(sacdata1)*sachd.delta;
        HEAD.spara(i).sta_no = i;
        HEAD.spara(i).staname = (deblank(sachd.kstnm'));
        HEAD.spara(i).Dstyle = nan;
        HEAD.spara(i).WLEN = nan;
        HEAD.spara(i).V = nan;
        HEAD.spara(i).sample = 1/sachd.delta;
        HEAD.spara(i).channel_n = 3;
        HEAD.spara(i).sta_lat = sachd.stla;
        HEAD.spara(i).sta_lon = sachd.stlo;
        HEAD.spara(i).sta_alt = nan;
        HEAD.spara(i).spara(i).Iangle  =sachd.az;     % 方位角
        HEAD.spara(i).spara(i).Oangle  =nan;     % 入射角
        HEAD.spara(i).spara(i).weight  =nan;
        HEAD.spara(i).spara(i).vmodel  =nan;     % 定位时使用的速度模型
        HEAD.spara(i).spara(i).datatype=nan;
        HEAD.spara(i).spara(i).seismometer=nan;            % 地震计名称
        HEAD.spara(i).spara(i).seismostyle=nan;
        for channel_i=1:HEAD.spara(i).channel_n
            HEAD.spara(i).Channel(channel_i).name=str2{10};          % 通道名称
            HEAD.spara(i).Channel(channel_i).no  =nan;          % 通道编号
            HEAD.spara(i).Channel(channel_i).gainfactor  =nan;% 增益因子
            HEAD.spara(i).Channel(channel_i).respondorder=nan;  % 响应阶数
        end
        HEAD.spara(i).isign=0;
        i = i + 1;
    end
end
HEAD.stn = i-1;
HEAD.recordlength = recordlength;
wave = WAVE;
head = HEAD;
end
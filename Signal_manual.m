function Signal_manual(hmain)
% Create table array
h=findall(0,'type','axes');
screen=get(0,'ScreenSize');
delete(h);
fig = hmain;
clearvars -except hmain screen
global ind
disp('signal earthquake manual starting...');
disp('load wave file...')
[tree, ~ , ~] = Read_xml('./config/config.xml');
wavetype = tree.data.waveform;
wavepath = Loadwave(wavetype);
if isequal(wavepath,0)
    disp('load fail!');
else
    switch wavetype
        case 1
            [~,eq_label,~] = fileparts(wavepath);
            [wave,head]=zjc_readevt(wavepath);
            stn = head.stn;t = table(stn,3);
        case 2
            [wave,head]=readsac_files(wavepath);
            eq_label = datestr(head.begintime,'yyyymmddHHMMSS');
            stn = head.stn;t = table(stn,3);
    end

    for i = 1:stn
        staname{i,1} = (char(head.spara(i).staname));
    end
    Pt=cell(stn,1);St=cell(stn,1);%yn_num=zeros(stn,1);
    T = table(staname,Pt,St);
    tcell = table2cell(T);
    fig = hmain;
    columnname =   {'Staname', 'S time','St length '};
    Columnwidth = {80 250 250};
    t = uitable(fig,'Data',tcell,'ColumnEditable',[false false false],'ColumnName', columnname,'ColumnWidth',Columnwidth,'CellEditCallback',@updata);
    t.Position=[0.5*screen(3) 0.05*screen(4) 0.28*screen(3) 0.5*screen(4)];
    %
    % % Create UI figure
    ax = axes(fig);
    ax.Units = 'pixels';
    ax.Position = [0.1*screen(3) 0.05*screen(4) 0.38*screen(3) 0.5*screen(4)];
    ax.Box = 'on';
    text(0.5,0.5,'Seismic wave show in here.')
    set(ax,'xtick',[]);
    set(ax,'ytick',[]);
    % % Cread another table
    c = uicontrol(fig,'Style','popupmenu');
    c.Position = [0.01*screen(3) 0.5*screen(4) 0.08*screen(3) 0.05*screen(4)];
    c.String = staname;
    c.Callback = @selection_sta;
    
    last_btn =  uicontrol(fig,'Style','pushbutton');
    last_btn.Position = [0.01*screen(3) 0.46*screen(4) 0.08*screen(3) 0.04*screen(4)];
    last_btn.BackgroundColor='c';
    last_btn.String = 'Last';
    last_btn.Callback = @last_sta;
    
    next_btn =  uicontrol(fig,'Style','pushbutton');
    next_btn.Position = [0.01*screen(3) 0.42*screen(4) 0.08*screen(3) 0.04*screen(4)];
    next_btn.BackgroundColor='c';
    next_btn.String = 'Next';
    next_btn.Callback = @next_sta;
    
    btn0 = uicontrol(fig,'Style','pushbutton');
    btn0.Position = [0.01*screen(3) 0.35*screen(4) 0.08*screen(3) 0.04*screen(4)];
    btn0.BackgroundColor='g';
    btn0.String = 'step 1:EQInFo';
    btn0.Callback = @eqinfo;
    
    btn = uicontrol(fig,'Style','pushbutton');
    btn.Position = [0.01*screen(3) 0.27*screen(4) 0.08*screen(3) 0.04*screen(4)];
    btn.BackgroundColor='g';
    btn.String = 'step 2:Mark';
    btn.Callback = @markps;
    
    btn2 = uicontrol(fig,'Style','pushbutton');
    btn2.Position = [0.01*screen(3) 0.19*screen(4) 0.08*screen(3) 0.04*screen(4)];
    btn2.BackgroundColor='g';
    btn2.String = 'step 3:Ready';
    btn2.Callback = @export;
    
    btn3 = uicontrol(fig,'Style','pushbutton');
    btn3.Position = [0.01*screen(3) 0.11*screen(4) 0.08*screen(3) 0.04*screen(4)];
    btn3.BackgroundColor='g';
    btn3.String = 'step 4:work';
    btn3.Callback = @work;
    
end
    function eqinfo(hObject,callbackdata)
        prompt0 = {                                                         % 对话框参数
            'earthquake time(year)',2020
            'earthquake time(month)',01
            'earthquake time(day)',22
            'earthquake time time(hour)',23
            'earthquake time(min)',40
            'earthquake time(sec)',21.37
            'earthquake lontitude',120.11
            'earthquake latitude',36.01
            'Magtitude',2.3
            'earthquake depth(km)',10
            };
        dlg0.width = 50;
        dlg0.title = 'earthquake information...';
        dlg0.auto = 0;
        para = Paradlg(prompt0,dlg0);
        setappdata(hmain,'eqinfo',para);
        %         ceqinfo = uicontrol(fig,'Style','text');
        %         ceqinfo.Position = [200 500 750 40];
        %         ceqinfo.String  = '地震信息、';
    end
    function selection_sta(src,event)
        val = c.Value;
        str = c.String;
        ind = val;
        title_str = sprintf('Begin Time: %s; Data length: %ss; \n Station: %s; Channal:%s',...
            datestr(head.begintime,31),num2str(head.recordlength),char(head.spara(ind).staname),...
            '\color[rgb]{0 0 1}UD \color[rgb]{0 1 0}EW \color[rgb]{1 0 0}NS');
        time = (0:length(wave{ind}(:,3))-1)/head.spara(ind).sample;
        temx = wave{ind}(:,1);temy = wave{ind}(:,2);temz = wave{ind}(:,3);
        Mx = max(abs(temx));			My = max(abs(temy));			Mz = max(abs(temz));
        x0 = Mx;		y0 = x0 + 1.1*(Mx+Mz);		z0 = y0 + 1.1*(My+Mz);
        time = (0:length(temx)-1)/head.spara(ind).sample;
        plot(time,temx+x0,'r',time,temy+y0,'g',time,temz+z0,'b','LineWidth',1);title(title_str);xlabel('Time[s]');ylabel('Velocity[um/s]');
        set(gca,'ytick',[]);
    end

function last_sta(src,event)
        val = c.Value;
        str = c.String;
        current_id = val;
        if current_id==1
            ind = 1;
            c.Value = ind;
%             set(c,'Value',str(ind));
        else
            ind = current_id-1;
            c.Value = ind;
%             set(c,'Value',str(ind));
        end
        title_str = sprintf('Begin Time: %s; Data length: %ss; \n Station: %s; Channal:%s',...
            datestr(head.begintime,31),num2str(head.recordlength),char(head.spara(ind).staname),...
            '\color[rgb]{0 0 1}UD \color[rgb]{0 1 0}EW \color[rgb]{1 0 0}NS');
        time = (0:length(wave{ind}(:,3))-1)/head.spara(ind).sample;
        temx = wave{ind}(:,1);temy = wave{ind}(:,2);temz = wave{ind}(:,3);
        Mx = max(abs(temx));			My = max(abs(temy));			Mz = max(abs(temz));
        x0 = Mx;		y0 = x0 + 1.1*(Mx+Mz);		z0 = y0 + 1.1*(My+Mz);
        time = (0:length(temx)-1)/head.spara(ind).sample;
        plot(time,temx+x0,'r',time,temy+y0,'g',time,temz+z0,'b','LineWidth',1);title(title_str);xlabel('Time[s]');ylabel('Velocity[um/s]');
        set(gca,'ytick',[]);
    end

    function next_sta(src,event)
        val = c.Value;
        str = c.String;
        ind = val;
        current_id = val;
        if current_id<length(str)
            ind = current_id+1;
            c.Value = ind;
%             set(c,'Value',str(ind));
        else
            ind = length(str);
            c.Value = ind;
        end
        title_str = sprintf('Begin Time: %s; Data length: %ss; \n Station: %s; Channal:%s',...
            datestr(head.begintime,31),num2str(head.recordlength),char(head.spara(ind).staname),...
            '\color[rgb]{0 0 1}UD \color[rgb]{0 1 0}EW \color[rgb]{1 0 0}NS');
        time = (0:length(wave{ind}(:,3))-1)/head.spara(ind).sample;
        temx = wave{ind}(:,1);temy = wave{ind}(:,2);temz = wave{ind}(:,3);
        Mx = max(abs(temx));			My = max(abs(temy));			Mz = max(abs(temz));
        x0 = Mx;		y0 = x0 + 1.1*(Mx+Mz);		z0 = y0 + 1.1*(My+Mz);
        time = (0:length(temx)-1)/head.spara(ind).sample;
        plot(time,temx+x0,'r',time,temy+y0,'g',time,temz+z0,'b','LineWidth',1);title(title_str);xlabel('Time[s]');ylabel('Velocity[um/s]');
        set(gca,'ytick',[]);
    end

    function markps(src,event)
        [xx,~] = ginput(2);
        while 1
            if xx(2)>xx(1)
                break;
            else
                [xx,~] = ginput(2);
            end
        end
        tp = datestr(datenum([head.begintime(1:5) head.begintime(6)+xx(1)]),31);
        ts = datestr(datenum([head.begintime(1:5) head.begintime(6)+xx(2)]),31);
        t.Data(ind,2)={tp};t.Data(ind,3)={ts};
    end

    function export(src,event)
        [tree, ~ , ~] = Read_xml('./config/config.xml');
        phapath = tree.path.phapath;
        filePermissions = 'w';
        if exist([phapath,eq_label,'.pha'],'file')
            delete([phapath,eq_label,'.pha']);
            value=getappdata(hmain,'eqinfo');
            if ~isempty(value)
                value_out.eqinfo = value;
                value_out.pha = t.Data;
                cell2txt([phapath,eq_label,'.pha'],value_out,filePermissions);
%                 cell2csv(['.\time\',eq_label,'.csv'],value,'%s',',',filePermissions);
%                 cell2csv(['.\time\',eq_label,'.csv'],t.Data,'%s',',',filePermissions);
            else
                msgbox('Please click EQInFo to input earthquake infomation firstly!')
                value=cell(10,1);
            end
        else
            value=getappdata(hmain,'eqinfo');
            if ~isempty(value)
                value_out.eqinfo = value;
                value_out.pha = t.Data;
                cell2txt([phapath,eq_label,'.pha'],value_out,filePermissions);
%                 cell2csv(['.\time\',eq_label,'.csv'],value,'%s',',',filePermissions);
%                 cell2csv(['.\time\',eq_label,'.csv'],t.Data,'%s',',',filePermissions);
            else
                msgbox('Please click EQInFo to input earthquake infomation firstly!');
                value=cell(10,1);
            end
            
        end
        %           value
        EVENT.origintime  = [value{1} value{2} value{3} value{4} value{5} value{6}];
        EVENT.epicenter   = [value{8} value{7}];
        EVENT.depth       = [value{9}];
        EVENT.mag         = [value{10}];
        EVENT.loc_stn     = [];
        %           EVENT.STA
        psnum = 1;
        [phanum,~]=size(t.Data);
        for i = 1:phanum
            if ~isempty(t.Data{i,2}) && ~isempty(t.Data{i,3})
                EVENT.STA(psnum).staname = (t.Data{i,1});
                EVENT.STA(psnum).Pg = datevec(t.Data{i,2});
                EVENT.STA(psnum).Sg = datevec(t.Data{i,3});
                psnum = psnum + 1;
            end
        end
        EVENT.loc_stn = length(EVENT.STA);
        setappdata(hmain,'phase',EVENT);
    end
    function work(hObject,callbackdata)
        if exist('h1000','var')
            close(h1000);
        end
        EVENT = getappdata(hmain,'phase');
        if isempty(EVENT)
            msgbox('Please Export phase time firstly.');
        else
            [tree, ~ , ~] = Read_xml('./config/config.xml');
            figpath = tree.path.fig_out;
            resultpath = tree.path.result_out;
            preconditioning.gatten = tree.preprocessing.gatten;
            preconditioning.gatten_b = tree.preprocessing.gatten_b;
            preconditioning.gatten_R = tree.preprocessing.gatten_R;
            preconditioning.Qf = [tree.preprocessing.Qf_a,tree.preprocessing.Qf_b];
            preconditioning.instrumentcase = tree.preprocessing.resp;
            preconditioning.site = tree.preprocessing.site;
            preconditioning.model = tree.model;
            preconditioning.delay = tree.signal.delay ;
            preconditioning.noise = tree.signal.noise;
            preconditioning.swind = tree.signal.swind;
            preconditioning.fftnum = tree.signal.fftnum;
            preconditioning.waveform = tree.data.waveform;
            switch preconditioning.waveform 
                case 1
                    waveform = 'evt';
                case 2
                    waveform = 'SAC';
                otherwise
                    waveform = 'unknow';
            end
            preconditioning.wavetype = tree.signal.wavetype; 
            switch preconditioning.wavetype 
                case 1
                    wave_type = 'P';
                case 2
                    wave_type = 'SH';
                otherwise
                    wave_type = 'unknow';          
            end
%             preconditioning.rptform = tree.data.reportform;
            preconditioning.invert = tree.invert;
            preconditioning.density = tree.ssp.density;
            preconditioning.velocity = tree.ssp.velocity;
            preconditioning.radiation = tree.ssp.radiation;
            SPECTRA = StationSpectra_z(wave,head,EVENT,preconditioning);
            SPECTRAnum = cellfun('isempty',SPECTRA);
            index = find(SPECTRAnum == 0);SPECTRA = SPECTRA(index(1:end-1));
            %% 反演
            switch preconditioning.model
                case 1
                    wavetitle= sprintf('eqlabel: %s; Seismic wave type: %s; Seismic wave format: %s; window = %ds',eq_label,wave_type,waveform,preconditioning.swind);
                    filterrange = tree.signal.frerange;
                    result = SpectraFit_Brune2(SPECTRA, preconditioning,eq_label,'v', filterrange,1);
                    if ~isempty(result)
                    save_result(EVENT,result,[eq_label,'-B1'],resultpath)
                    resulttitle = sprintf('%s (@Frequency range. %s-%sHz; @eqinfo. %s)\n omg=%s;fc=%.2fHz;\n Mo=%sNm; Mw=%.2f; R=%.2fm;\n StressDrop=%sMPa; Apparent stress=%sMPa', ...
                        eq_label,num2str(filterrange(1)),num2str(filterrange(2)),...
                        [datestr(EVENT.origintime,31),',',num2str(EVENT.epicenter(1)),',',num2str(EVENT.epicenter(2)),',',num2str(EVENT.mag)],...
                        result.SPa(1,1),result.SPa(1,2),result.SSP(1),result.SSP(2),result.SSP(3),result.SSP(4),result.SSP(5));
                    h1001 = figure();screen = get(0,'ScreenSize');set(h1001,'Position',[screen(1) screen(2) screen(3)/1.2 screen(4)/1.2]);
                    ha = tight_subplot(2,2,[.1 .04],[.1 .05],[.05 .05]);
                    axes(ha(1)),wave_plot(SPECTRA,wavetitle);
                    axes(ha(2)),info_plot(SPECTRA,EVENT);axis equal;
                    axes(ha(3)),spec_plot(SPECTRA,'d',filterrange);%plotDiagnostics(result.mdl);
                    axes(ha(4)),fit_plot(result);px = xlim;py=ylim;
                    title(resulttitle);
                    set(gca,'LineWidth',1.5)
%                     title(resulttitle,'position',[(px(2)-px(1))/20 (py(2)-py(1))/6]);
%                     axes(ha(4)),axis off;text(0,0.5,resulttitle,'FontSize',12)
                    print(gcf,'-dpng','-r300',[figpath,eq_label,'.png']);
                    saveas(gcf,[figpath,eq_label,'.fig']);
                    disp([eq_label,' work done!']);
                    else
                         disp([eq_label,' is existing problem!']);
                    end
                case 2
                    wavetitle= sprintf('eqlabel: %s; Seismic wave type: %s; Seismic wave format: %s; window = %ds',eq_label,wave_type,waveform,preconditioning.swind);
                    filterrange = tree.signal.frerange;
                    result = SpectraFit_Brune2(SPECTRA, preconditioning,eq_label,'v', filterrange,1);
                    if ~isempty(result)
                    save_result(EVENT,result,[eq_label,'-B2'],resultpath)
                    resulttitle = sprintf('%s (@Frequency range. %s-%sHz; @eqinfo. %s)\n omg=%s;fc=%.2fHz;\n Mo=%sNm; Mw=%.2f; R=%.2fm;\n StressDrop=%sMPa; Apparent stress=%sMPa', ...
                        eq_label,num2str(filterrange(1)),num2str(filterrange(2)),...
                        [datestr(EVENT.origintime,31),',',num2str(EVENT.epicenter(1)),',',num2str(EVENT.epicenter(2)),',',num2str(EVENT.mag)],...
                        result.SPa(1,1),result.SPa(1,2),result.SSP(1),result.SSP(2),result.SSP(3),result.SSP(4),result.SSP(5));
                    h1001 = figure();screen = get(0,'ScreenSize');set(h1001,'Position',[screen(1) screen(2) screen(3)/1.2 screen(4)/1.2]);
                    ha = tight_subplot(2,2,[.1 .04],[.1 .05],[.05 .05]);
                    axes(ha(1)),wave_plot(SPECTRA,wavetitle);
                    axes(ha(2)),info_plot(SPECTRA,EVENT);axis equal;
                    axes(ha(3)),spec_plot(SPECTRA,'d',filterrange);%plotDiagnostics(result.mdl);
                    axes(ha(4)),fit_plot(result);px = xlim;py=ylim;
                    title(resulttitle);
                    set(gca,'LineWidth',1.5)
%                     title(resulttitle,'position',[(px(2)-px(1))/20 (py(2)-py(1))/6]);
%                     axes(ha(4)),axis off;text(0,0.5,resulttitle,'FontSize',12)
                    print(gcf,'-dpng','-r300',[figpath,eq_label,'.png']);
                    saveas(gcf,[figpath,eq_label,'.fig']);
                    disp([eq_label,' work done!']);
                    else
                         disp([eq_label,' is existing problem!']);
                    end
                case 3
                    wavetitle= sprintf('eqlabel: %s; Seismic wave type: %s; Seismic wave format: %s; window = %ds',eq_label,wave_type,waveform,preconditioning.swind);
                    filterrange = tree.signal.frerange;
                    switch tree.invert
                        case 1
                            result = SpectraFit_HC2(SPECTRA, preconditioning,eq_label,'v', filterrange,1);
                        case 2
                            result = SpectraFit_HC2(SPECTRA, preconditioning,eq_label,'v', filterrange,1);
                    end
                    
                    
                    if ~isempty(result)
                    save_result(EVENT,result,[eq_label,'-HC'],resultpath)
                    resulttitle = sprintf('%s (@Frequency range. %s-%sHz; @eqinfo. %s)\n omg=%s;fc=%.2fHz;\n Mo=%sNm; Mw=%.2f; R=%.2fm;\n StressDrop=%sMPa; Apparent stress=%sMPa', ...
                        eq_label,num2str(filterrange(1)),num2str(filterrange(2)),...
                        [datestr(EVENT.origintime,31),',',num2str(EVENT.epicenter(1)),',',num2str(EVENT.epicenter(2)),',',num2str(EVENT.mag)],...
                        result.SPa(1,1),result.SPa(1,2),result.SSP(1),result.SSP(2),result.SSP(3),result.SSP(4),result.SSP(5));
                    h1001 = figure();screen = get(0,'ScreenSize');set(h1001,'Position',[screen(1) screen(2) screen(3)/1.2 screen(4)/1.2]);
                    ha = tight_subplot(2,2,[.1 .04],[.1 .05],[.05 .05]);
                    axes(ha(1)),wave_plot(SPECTRA,wavetitle);
                    axes(ha(2)),info_plot(SPECTRA,EVENT);axis equal;
                    axes(ha(3)),spec_plot(SPECTRA,'d',filterrange);%plotDiagnostics(result.mdl);
                    axes(ha(4)),fit_plot(result);px = xlim;py=ylim;
                    title(resulttitle);
                    set(gca,'LineWidth',1.5)
%                     title(resulttitle,'position',[(px(2)-px(1))/20 (py(2)-py(1))/6]);
%                     axes(ha(4)),axis off;text(0,0.5,resulttitle,'FontSize',12)
                    print(gcf,'-dpng','-r300',[figpath,eq_label,'.png']);
                    saveas(gcf,[figpath,eq_label,'.fig']);
                    disp([eq_label,' work done!']);
                    else
                       disp([eq_label,' is existing problem!']); 
                    end
                    
            end

        end
    end

    function updata(hObject,callbackdata)
        numval = eval(callbackdata.EditData);
        r = callbackdata.Indices(1);
        c = callbackdata.Indices(2);
        hObject.Data{r,c} = numval;
    end
end
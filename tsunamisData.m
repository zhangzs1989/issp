function tsunamisData
% Create table array
% clear;
[wave,head]=zjc_readevt('./ysd/201006061848.evt');
stn = head.stn;t = table(stn,3);
for i = 1:stn
    staname{i,1} = (char(head.spara(i).staname));
end
Pt=zeros(stn,1);St=zeros(stn,1);yn=zeros(stn,1);yn=logical(yn);
t = table(staname,Pt,St,yn);

% Create UI figure
fig = uifigure;
fig.Position(3:4) = [900 360];
% Create table UI component
uit = uitable(fig);
uit.Data = t;
uit.ColumnSortable = true;
uit.ColumnEditable = [false true true true];
uit.Position(3) = 320;
uit.DisplayDataChangedFcn = @updatePlot;
% Create bubble chart
ax = uiaxes(fig);
ax.Position(1) = 350;ax.Position(3) = 500;
ax.XLabel.String = 'Time/s';
ax.YLabel.String = 'Velocity/cm/s';
plot(ax,wave{1}(:,3),'LineWidth',1);
selid1 = t.yn;
% Update the bubble chart when table data changes
    function updatePlot(src,event)
        pause(1);
        t = uit.Data;
        selid2 = t.yn;
        ind=selid1~=selid2;
        selid1 = selid2;
        plot(ax,wave{ind}(:,3),'LineWidth',1);
    end

end
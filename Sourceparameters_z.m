function [m0,mw,r,sd,app] = Sourceparameters_z(omg,fc,vel,pre)
density = pre.density;velocity = pre.velocity;radition = pre.radiation;
factor=4*pi*density*velocity^3/(2*radition)*10^9;
m0=factor*omg;
r=2.34*(velocity*1000)/(2*pi*fc);
sd=7/16*m0/(r)^3/10^6;
% % factor=(4*pi*density*velocity^3)/(2*sqrt(2/5))*10^15;
% % m0=factor*omg;
mw=2/3*log10(m0) -6.033;
% % mw=(0.6667*log10(m0))-10.7;
% % r=(2.34*(velocity*1000))/(2*pi*fc);
% % m0=m0/10000000;                                  % dyne-cm to Nm
% % sd=7/16*m0/(r)^3/10^6;
Es=8*pi*density*velocity*sum(vel.*vel)*10^5;
% m0=m0*10000000; %Nm to dyne-cm
app = 3.5*10^4*Es/m0/1000000;
end
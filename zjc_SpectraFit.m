function SPa=zjc_SpectraFit(fv, spec, chrSpectype, initValue, plot_or_not)
%-------------------------------------------------------------------------
%               PROGRAM TO FIT SOURCE MODEL 
% Brune model with high-cut fmax model fitted to source spectrum
% Input:
%	fv     - discrete frequencies.
%	spec   - observed spectra.
%	chrSpectype - 'v', 'd' or 'a'; default is 'v'. Correponding to spectra
%				   velocity, displacement or acceleration. 
%	initValue   - initial values for theoretical model, a structure with 5
%				  members: .fc0, .fmax0, .index1, .index2, .slope;
%	plot_or_not - % =0, no plots; =1(default), plot figures.
% Output:
%	SPa    - spectra parameters, a structure.
%		.omega
%		.omega_intervals
%		.fc
%		.fc_intervals
%		.gamma
%		.gamma_intervals
%		.fmax
%		.fmax_intervals
%		.p
%		.p_intervals
%=========================================================================
% Jianchang ZHENG @ Jinan, Sept 15, 2015.
% Modified on Aug.14, 2015 @ Room 408# Plaza C, 2066 Ganxi Rd, Jinan.
% completed on May 26, 2016@ STEC, National Chung Cheng University.
global eventlabel

Pi2=6.28318530718;
SPa=[];
nfv=numel(fv);
%-------------------------------------------------------------------------
if ~exist('chrSpectype','var')
	chrSpectype='v'; %velocity
end
switch chrSpectype
	case 'a'
		acc  =spec;
		vel  =acc./(2*pi*fv).^1;
		displ=vel./(2*pi*fv).^1;
		tecc =vel.*(2*pi*fv).^2.5;
	case 'v'
		vel  =spec;
		displ=vel./(2*pi*fv).^1;
		acc  =vel.*(2*pi*fv).^1;
		tecc =vel.*(2*pi*fv).^2.5;
	case 'd'
		displ=spec;
		vel=displ.*(2*pi*fv).^1;
		acc  =vel.*(2*pi*fv).^1;
		tecc =vel.*(2*pi*fv).^2.5;
	otherwise
end
if ~exist('initValue','var')
	[fc0,fmax0,index1,index2,slope]=zjc_initCornerPoints(fv,acc);
else
	fc0   =initValue.fc0; 
	fmax0 =initValue.fmax0; 
	index1=initValue.index1;
	index2=initValue.index2;
	slope =initValue.slope;
end

if ~exist('plot_or_not','var')
	plot_or_not =1;  % =0, no plots;
end



%-------------------------------------------------------------------------
%	高频衰减系数的稳健回归
    p0=abs(slope(3));
    avacc =log10(acc(index1:index2));
    omega0=mean(avacc)/(2*pi*fc0).^2;
	%
    ind=index1:index2;
	coeffs=log10(fv(ind));
	y=log10(displ(ind));
	[brob,stats] = robustfit(coeffs,y);  % Robust congression fit
	%figure; polytool(x,y);         	 % 
										 % 95% confidence interval
	bcirob1 = nlparci(brob,stats.resid,'cov',stats.covb);
										 % alpha=0.05(default)
	gamma0=-brob(2);
	gamma_intervals=fliplr(-bcirob1(2,:));

% ---------------------------------------------------------------------
%	经典Brune模型拟合
	ind=1:index2;
	f=fv(ind);
	d=displ(ind);
% 	g1 = fittype(['a/(1+(x/b)^' sprintf('%f',gamma0) ');']);
	fun_Brune = @(a,b,x) a./(1+(x./b).^gamma0);
	g1 =@(a,b,x) log10(fun_Brune(a,b,x));
	[fitobj_1,g1of] = fit(f,log10(d),g1,'StartPoint',[omega0,fc0],...
							'Lower',[min(displ) min(fv)],...
							'Upper',[max(displ) fmax0  ],...
							'Robust','on');
	CI = confint(fitobj_1,0.95);		 % 95% confidence intervals
	omega =fitobj_1.a;
	fc    =fitobj_1.b;
	gamma =gamma0;
	omega_intervals =CI(:,1);
	fc_intervals    =CI(:,2);
% 	gamma_intervals =CI(:,3);
	a=fitobj_1.a;
	b=fitobj_1.b;
	c=gamma0;
	figure(1000);	subplot(221); plot(fv,fun_Brune(a,b,fv));
					subplot(223); plot(fv,(Pi2.*fv).^2.*fun_Brune(a,b,fv));

% ------------------------------------------------------------------------
%	高频截止模型拟合
    ind=fv<40 & fv>=0.1;
%     ind(1)=false;
    xdata=fv(ind);
	ydata=acc(ind)./((Pi2.*xdata).^2.*fun_Brune(a,b,xdata));
    
%     ydata=(Pi2.*xdata).^2.*fun_Brune(a,b,xdata);
	fun_Boore=@(coeffs,f) (1+(f./coeffs(1)).^coeffs(2)).^(-1);
	funfit   =@(coeffs,f) fun_Boore(coeffs,f);
	
    coeffs0=[fmax0 p0];
%     lb=[omega_intervals(1)  fc_intervals(1) gamma_intervals(2) fmax0-2 0];
%     ub=[omega_intervals(2)  fc_intervals(2) gamma_intervals(1) fmax0+2 10];
	%lb =[fmax0-2 0];
	%ub =[15 10];
    %Options = optimset('Display','off','MaxIter',200,'TolX',1.0E-8);
    %[x1,resnorm,residual,exitflag,output,lambda,jacobian] = lsqcurvefit(fun_d,x0,xdata, displ(ind));
    %[x2,resnorm,residual,exitflag,output,lambda,jacobian] = lsqcurvefit(fun_v,x0,xdata, vel(ind));
    %[x3,resnorm,residual,exitflag,output,lambda,jacobian] = lsqcurvefit(fun_a,x0,xdata, acc(ind));
	%[coeffs,resnorm,residual,exitflag,output,lambda,jacobian] = lsqcurvefit(funfit,coeffs0,xdata,ydata,lb,ub,Options);
    Options = statset('Robust','on','RobustWgtFun','bisquare');
	[coeffs,residual,jacobian]=nlinfit(xdata,ydata,funfit,coeffs0,Options);
    conf = nlparci(coeffs,residual,'jacobian',jacobian);
	[ypred,delta]=nlpredci(funfit, xdata, coeffs, residual, jacobian);
	if plot_or_not
		figure(1001); hold on;
		loglog (xdata,ydata);
		loglog (xdata,funfit(coeffs,xdata),'m');
		loglog ([xdata xdata],[ypred-delta ypred+delta],'g:');
		title('Acceleration');
	end
    %
	fun_d=@(coeffs,f)                fun_Brune(a,b,f).*funfit(coeffs,f);
	fun_v=@(coeffs,f) (Pi2.*f)     .*fun_Brune(a,b,f).*funfit(coeffs,f);
	fun_a=@(coeffs,f) (Pi2.*f).^2.0.*fun_Brune(a,b,f).*funfit(coeffs,f);
	fun_t=@(coeffs,f) (Pi2.*f).^3.5.*fun_Brune(a,b,f).*funfit(coeffs,f);
	
	if plot_or_not
	    figure(1000); 
	    subplot(221); plot(fv,fun_d(coeffs,fv),'k');
	    subplot(222); plot(fv,fun_v(coeffs,fv),'k');
	    subplot(223); plot(fv,fun_a(coeffs,fv),'k');
		plot([xdata xdata],[(ypred-delta).*fun_Brune(a,b,xdata),(ypred+delta).*fun_Brune(a,b,xdata)],'--');
	    figure(1000); subplot(224); plot(fv,fun_t(coeffs,fv),'k');
        suptitle(eventlabel);
% 		saveas(gcf,['.\results\fmax_1000.' eventlabel '.fig']);
% 		saveas(gcf,['.\results\fmax_1000.' eventlabel '.emf']);
	end

% ----------------------------------------------------------------------
% 利用上面给出的置信区间，进一步约束拟合。Uncomplete.
%omega=coeffs(1); %omg_intervals  =conf(1,:);
%fc   =coeffs(2); %fc_intervals   =conf(2,:);
%gamma=coeffs(3); %gamma_intervals=conf(3,:);
fmax =coeffs(1); fmax_intervals =conf(1,:);
p    =coeffs(2); p_intervals    =conf(2,:);

%%======================================================================
	%SPa.omega  =omega;
	%SPa.omega_intervals=omega_intervals;
	%SPa.fc   =fc;
	%SPa.fc_intervals = fc_intervals;
	%SPa.fmax =fmax;
	%SPa.fmax_intervals=fmax_intervals;
	%SPa.gamma=gamma;
	%SPa.gamma_intervals=gamma_intervals;
	%SPa.p    =p;
	%SPa.p_intervals=p_intervals;
	SPa=[omega omega_intervals(1) omega_intervals(2); ...
		 fc	   fc_intervals(1)    fc_intervals(2);    ...
		 gamma gamma_intervals(1) gamma_intervals(2); ...
		 fmax  fmax_intervals(1)  fmax_intervals(2);  ...
		 p     p_intervals(1)     p_intervals(2) ];
	SPa=SPa';
	

%end of files.
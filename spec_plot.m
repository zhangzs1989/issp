function spec_plot(SPECTRA,type,frerange)
Spectranum = length(SPECTRA);
str_tmp='';
spec_ave=0;
switch type
    case 'v'
    case 'd'
        for i = 1:Spectranum
            fv=SPECTRA{i}.DataD(:,1);
            spec_ave=spec_ave+SPECTRA{i}.DataD(:,4);
            ind=fv<=frerange(2) & fv>=frerange(1);
%             str_tmp=[str_tmp sprintf('%s,',SPECTRA{i}.staname)];
            str_tmp{i}=char(sprintf('%s',SPECTRA{i}.staname)); 
            loglog(SPECTRA{i}.DataD(ind,1),SPECTRA{i}.DataD(ind,4));hold on;
        end
    case 'a'
end
%     loglog(fv(ind),spec_ave(ind)./Spectranum,'k','LineWidth',2);
    xlabel('Frequency[Hz]');ylabel('Observed Station Displacement Spectrum');
    hleg = legend(str_tmp);
%     str_tmp=strrep(str_tmp(12:end),',',''',''');
% 	str_tmp=strcat(char(39),str_tmp,char(39));
%     str_tmp=[str_tmp sprintf('%s,','AVE')];
% 	str_tmp=strcat('hleg=legend(',str_tmp,');');
% 	eval(str_tmp);
    try
        set(hleg,'FontName','Verdana','FontSize',7.0,'TextColor',[.3,.2,.1],...
			 'Location','SouthWest','NumColumns',ceil(Spectranum/10));
    catch
%         set(hleg,'FontName','Verdana','FontSize',7.0,'TextColor',[.3,.2,.1],...
% 			 'Location','SouthWest','NumColumns',ceil(Spectranum/10));
    end
    set(gca,'linewidth',1.2);
    
% str_tmp=strcat(char(39),str_tmp,char(39));
% str_tmp=strcat('hleg=legend(',str_tmp,');');
end
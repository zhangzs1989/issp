function Set_Pre_Para()
try
[tree, RootName , ~] = Read_xml('./config/config.xml');
prompt0 = {                                                         % 对话框参数
    'Instrument response【1-\it{default}；2-\it{remove Instrument response}】',tree.preprocessing.resp
    'geometric falloff【1-\it{G(R)=R^{-1}}；2-\it{correction}】',tree.preprocessing.gatten
    'Three-stage geometric attenuation correction coefficient',tree.preprocessing.gatten_b
    'Three-segment geometric attenuation correction distance parameter',tree.preprocessing.gatten_R
    'Inelastic decay【Q(f)=a*f^b, a】',tree.preprocessing.Qf_a
    'Inelastic decay【Q(f)=a*f^b, b】',tree.preprocessing.Qf_b
    'Site response【1-\it{default};2-\it{load}】',tree.preprocessing.site
%     '是否滤波【1-进行滤波；0-不滤波】',tree.preprocessing.filter
};
% defaultans = {'1','2','3','4'};
% inputdlg(prompt0,'预处理',1,defaultans)
dlg0.width = 70;
dlg0.title = 'Preprocessing';
dlg0.auto = 0;
para = Paradlg(prompt0,dlg0);
try 
set_resp = para{1};set_GR = para{2};set_GRb=para{3};set_GRR=para{4};
set_Qf1 = para{5}; set_Qf2 = para{6};set_site = para{7};
% set_filter = para{6};
% if set_filter == 1
%     [filtername,filterpath]=uigetfile('.mat');
%     coef = load([filterpath,filtername]);
%     if ~isstruct(coef) || ~isfield(coef,'SOS')  || ~isfield(coef,'G')
%         msgbox('导入的滤波器参数不正确，请检查后重新导入！')
%         set_file_path = 0;
%     else
%         set_file_path = [filterpath,filtername];
%     end
% else
%    set_file_path = 0;
% end
catch ErrorInfo
    msgbox(ErrorInfo.message)
end
try
tree.preprocessing.resp = set_resp;
tree.preprocessing.gatten = set_GR;
tree.preprocessing.gatten_b=set_GRb;
tree.preprocessing.gatten_R=set_GRR;
tree.preprocessing.site = set_site;
tree.preprocessing.Qf_a = set_Qf1;
tree.preprocessing.Qf_b = set_Qf2;
% tree.preprocessing.filter = set_file_path;
Write_xml('./config/config.xml',tree,RootName);
catch ErrorInfo
    msgbox(ErrorInfo.message) 
end
end
function fit_plot(result)
%UNTITLED4 Summary of this function goes here
%   Detailed explanation goes here
fv= result.obs(:,1);displ=result.obs(:,2);spektrd=result.the(:,2);fc =result.fc(1); spektrd0=result.fc(2);
spektrd1 = result.the1(:,2);spektrd2 = result.the2(:,2);
% noise = result.noise(:,2);
hssp = loglog(fv,displ,'m');hold on;
% hnoise = loglog(fv,noise,'color',[.5 .5 .5]);
htheory = loglog(fv,spektrd,'b','LineWidth',2);grid on;hold on;
hcont = loglog(fv,spektrd1,'g--','LineWidth',1.2);hold on;
loglog(fv,spektrd2,'g--','LineWidth',1.2);hold on;
plot(fc,spektrd0,'.', 'MarkerSize',30,'MarkerEdgeColor','g','MarkerFaceColor','g');
plot(fc,spektrd0,'k+', 'MarkerSize',25,'LineWidth',1.2);
xlabel('Frequency[Hz]');ylabel('DISPLACEMENT SPECTRA');
set(gca,'linewidth',1.2);
lgd = legend([hssp,htheory,hcont],'Observed source spectrum','Theoretical source spectrum','Confidence interval','Location','SouthWest');
end
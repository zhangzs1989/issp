function zr=zjc_Atkinson(r,Precondition)
%	3 segment geometrical spreading operator
Precondition=Precondition;
R = Precondition.gatten_b;
b = Precondition.gatten_R;
Q=Precondition.Qf;

R01=R(1); %km, geometrical attenuation
R02=R(2);
Q0=Q(1); alfa=Q(2);
b1=b(1);
b2=b(2);
b3=b(3);
Vk = 8.5;
Vs = 3.2;
zr=1.0;
%fun_Q = @(r,x) exp((pi*r.*f^(1.0-alfa))/(Q0*Vs));
if r<=R01
    zr=(r^b1);
elseif r <= R02
    zr=(R01^b1)*((r/R01)^b2);
else
    zr=(R01^b1)*((R02/R01)^b2)*((r/R02)^b3);
end
%fun_P = @(r,x) zr*fun_Q(r,x);
end
					

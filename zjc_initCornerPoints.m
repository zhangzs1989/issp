function [fc0,fmax0,ind1,ind2,bv]=zjc_initCornerPoints(fv,acc)
hwait=waitbar(0,'Waiting......');
set(hwait,'Name','searching initial value ... ');
warning('off','all');
ind=find(fv<=30,1,'last');
xdata=log10(fv(1:ind));
ydata=log10(acc(1:ind));
ind=find(xdata>=1,1,'first');
j_start=find(xdata>=0,1,'first');

%fp=fopen('zzz.dat','wt');
ER_V=nan(numel(xdata),numel(ydata));

%for i=3:numel(xdata)-3-3
for i=3:ind
	progress_r=i/(ind);
	str_tmp=sprintf('%.2f%% complete, waiting......',progress_r*100);
	waitbar(progress_r,hwait,str_tmp);
	if i+3>j_start
		j_start=i+3;
	end
	for j=j_start:numel(xdata)-3;
		a=[i,j];
		fval=zjc_fitMultiSeg3(a,xdata,ydata);
		%fprintf(fp,'%d %d %.6f\n',i,j,fval);
		ER_V(i,j)=fval;
	end
end
%fclose(fp);
close(hwait);

%figure;surface(ER_V');
% save('zzz.mat','ER_V','-mat');

V=ER_V(~isnan(ER_V));
[ind1,ind2]=find(ER_V==min(V),1,'first');
fc0  =fv(ind1);
fmax0=fv(ind2);
a =[ind1,ind2];
[~,bv]=zjc_fitMultiSeg3(a,xdata,ydata,1);
